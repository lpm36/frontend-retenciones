import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportarRetencionComponent } from './exportar-retencion.component';

describe('ExportarRetencionComponent', () => {
  let component: ExportarRetencionComponent;
  let fixture: ComponentFixture<ExportarRetencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportarRetencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportarRetencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
