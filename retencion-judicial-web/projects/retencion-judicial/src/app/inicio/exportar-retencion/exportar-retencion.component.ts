import { Component, OnInit } from '@angular/core';
import { ServiciosRetencionesJudicialesService } from '../../util/services/servicios-retenciones-judiciales.service';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-exportar-retencion',
  templateUrl: './exportar-retencion.component.html',
  styleUrls: ['./exportar-retencion.component.css']
})
export class ExportarRetencionComponent implements OnInit {

  ngFechaInicial: Date;
  ngFechaFinal: Date;
  yearRange: string;
  es: any;
  maxDate: Date;
  minDate: Date;
  imagenCargando: boolean;

  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService
  ) { }

  ngOnInit() {
    this.cargarDatosIniciales();
  }

  cargarDatosIniciales() {
    this.cargarFecha();
    this.cargarMesesEsp();
    this.fechaMaxMin();
  }

  cargarFecha() {
    this.yearRange = "1994" + ":" + (new Date().getFullYear().toString());
  }

  devolverCampos() {
    let valores: string[] = [];
    let fchIni: string = this.validarFechaInical();
    let fchFin: string = this.validarFechaFinal();
    (fchIni != "") ? valores.push(fchIni) : "";
    (fchFin != "") ? valores.push(fchFin) : "";
    this.generarArchivo(parseInt(fchIni), parseInt(fchFin));
  }

  cargarMesesEsp() {
    this.es = {
      firstDayOfWeek: 1,
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      today: 'Hoy',
      clear: 'Borrar'
    }
  }

  fechaMaxMin() {
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let nextMonth = (month === 11) ? 0 : month;
    let nextYear = (nextMonth === 0) ? year : year;
    let prevMonth = (month === 0) ? 11 : month;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    this.maxDate = new Date();
    this.maxDate.setMonth(nextMonth);
    this.maxDate.setFullYear(nextYear);
    this.minDate = new Date();
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);
  }

  validarDatosFechaFinal() {
    this.ngFechaFinal = null;
    this.ngFechaInicial = (this.ngFechaInicial !== undefined && this.ngFechaInicial !== null) ? this.ngFechaInicial : new Date();
    let today = this.ngFechaInicial;
    let day = today.getDate();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    this.minDate.setDate(day);
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);
  }

  validarFechaFinal() {
    if (this.ngFechaFinal !== undefined && this.ngFechaFinal !== null) {
      let diaFchFin = (this.ngFechaFinal.getDate() < 10) ? "0" + this.ngFechaFinal.getDate() : this.ngFechaFinal.getDate().toString();
      let mesFchFin = ((this.ngFechaFinal.getMonth() + 1) < 10) ? "0" + (this.ngFechaFinal.getMonth() + 1) : (this.ngFechaFinal.getMonth() + 1).toString();
      let anioFchFin = this.ngFechaFinal.getFullYear().toString();
      return (anioFchFin + mesFchFin + diaFchFin);
    } else {
      return "";
    }
  }

  validarFechaInical() {
    if (this.ngFechaInicial !== undefined && this.ngFechaInicial !== null) {
      let diaFchIni = (this.ngFechaInicial.getDate() < 10) ? "0" + this.ngFechaInicial.getDate() : this.ngFechaInicial.getDate().toString();
      let mesFchIni = ((this.ngFechaInicial.getMonth() + 1) < 10) ? "0" + (this.ngFechaInicial.getMonth() + 1) : (this.ngFechaInicial.getMonth() + 1).toString();
      let anioFchIni = this.ngFechaInicial.getFullYear().toString();
      return (anioFchIni + mesFchIni + diaFchIni);
    } else {
      return "";
    }
  }

  generarArchivo(fechaInicial: number, fechaFinal: number) {
    this.imagenCargando = true;
    this._servicioRJ.getDescargarRetencionesJudiciales(fechaInicial, fechaFinal, "").subscribe(dato => {
      var byteArray = new Uint8Array(dato);
      let today = new Date();
      let day = (today.getDate() < 10) ? "0" + today.getDate() : today.getDate();
      let month = ((today.getMonth() + 1) < 10) ? "0" + (today.getMonth() + 1) : (today.getMonth() + 1);
      let year = today.getFullYear();
      let nomDocumento: string = 'RETENCIONES_JUDICIALES_' + year + month + day + ".csv"
      saveAs(new Blob([byteArray], { type: '*/*' }), nomDocumento);
      this.imagenCargando = false;
    });
  }

}
