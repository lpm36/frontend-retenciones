import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaRetencionesComponent } from './vista-retenciones.component';

describe('VistaRetencionesComponent', () => {
  let component: VistaRetencionesComponent;
  let fixture: ComponentFixture<VistaRetencionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaRetencionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaRetencionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
