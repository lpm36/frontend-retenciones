import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleRetencionComponent } from './detalle-retencion.component';

describe('DetalleRetencionComponent', () => {
  let component: DetalleRetencionComponent;
  let fixture: ComponentFixture<DetalleRetencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleRetencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleRetencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
