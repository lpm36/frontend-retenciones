import { Component, OnInit } from '@angular/core';
import { RetencionJudicial } from '../../util/interfaces/retencion-judicial';
import { Router } from '@angular/router';
import { ServiciosRetencionesJudicialesService } from '../../util/services/servicios-retenciones-judiciales.service';
import { forkJoin } from 'rxjs';
import { TipoMoneda } from '../../util/interfaces/tipo-moneda';
import { RetencionAgencia } from '../../util/interfaces/retencion-agencia';
import { ListaValor } from '../../util/interfaces/lista-valor';
import { EntidadFinanciera } from '../../util/interfaces/entidad-financiera';
import { EntidadAseguradora } from '../../util/interfaces/entidad-aseguradora';

@Component({
  selector: 'app-detalle-retencion',
  templateUrl: './detalle-retencion.component.html',
  styleUrls: ['./detalle-retencion.component.css']
})
export class DetalleRetencionComponent implements OnInit {

  retJud: RetencionJudicial = new RetencionJudicial();
  arragloTipoMoneda: TipoMoneda[] = [];
  arregloAgencias: RetencionAgencia[] = [];
  arrayFormaPago: ListaValor[] = [];
  arrayEntidadFinanciera: EntidadFinanciera[] = [];
  arrayEncargadoRetencion: ListaValor[] = [];
  arrayEntidadAseguradora: EntidadAseguradora[] = [];
  imagenCargando: boolean = false;
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cargarDatos();
    this.cargarCombo2();

  }

  cargarDatos() {
    let objeto = sessionStorage.getItem('reteJudi');
    if (objeto !== undefined && objeto !== null) {
      this.retJud = JSON.parse(objeto);
      console.log('this.retJud: ', this.retJud);

      sessionStorage.removeItem('reteJudi');
    } else {
      this.router.navigate(['listar-retenciones']);
    }
  }

  regresar() {
    this.router.navigate(['listar-retenciones']);
  }

  cargarCombos1() {
  }

  cargarCombo2() {
    this.imagenCargando = true;
    const getForPago = this._servicioRJ.getListaFormaPago();
    const getDeposito = this._servicioRJ.getEntidadFinanciearDeposito();
    const getEncargadoRet = this._servicioRJ.getListaEncargadoRetencion();
    const getEntidadesAseguradoras = this._servicioRJ.getEntidadAseguradoras();
    forkJoin([getForPago, getDeposito, getEncargadoRet, getEntidadesAseguradoras]).subscribe(result => {
      this.arrayFormaPago = result[0];
      this.arrayEntidadFinanciera = result[1];
      this.arrayEncargadoRetencion = result[2];
      this.arrayEntidadAseguradora = result[3];
      this.imagenCargando = false;
    });
  }
}
