import { NgModule } from '@angular/core';
import { ListarRetencionesComponent } from './listar-retenciones/listar-retenciones.component';
import { CompartidoModule } from '../util/compartido/compartido.module';
import { MaterialModule } from '../util/material/material.module';
import { PrimengModule } from '../util/primeng/primeng.module';
import { ExportarDatosComponent } from './listar-retenciones/exportar-datos/exportar-datos.component';
import { EditarComponent } from './listar-retenciones/editar/editar.component';
import { DejaSinEfectoComponent } from './listar-retenciones/editar/deja-sin-efecto/deja-sin-efecto.component';
import { ModificarComponent } from './listar-retenciones/editar/modificar/modificar.component';
import { MedioPagoComponent } from './listar-retenciones/editar/modificar/medio-pago/medio-pago.component';
import { DatoCobranteComponent } from './listar-retenciones/editar/modificar/dato-cobrante/dato-cobrante.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormularioComponent } from './listar-retenciones/editar/deja-sin-efecto/formulario/formulario.component';
import { NuevaRetencionComponent } from './listar-retenciones/nueva-retencion/nueva-retencion.component';
import { InicioNuevaRetencionComponent } from './listar-retenciones/nueva-retencion/inicio-nueva-retencion/inicio-nueva-retencion.component';
import { DatosDemandadoComponent } from './listar-retenciones/nueva-retencion/datos-demandado/datos-demandado.component';
import { DatosDemandanteComponent } from './listar-retenciones/nueva-retencion/datos-demandante/datos-demandante.component';
import { IndicadoresRetencionComponent } from './listar-retenciones/nueva-retencion/indicadores-retencion/indicadores-retencion.component';
import { InicioBuscarPersonaComponent } from './listar-retenciones/nueva-retencion/inicio-nueva-retencion/inicio-buscar-persona/inicio-buscar-persona.component';
import { ExportarRetencionComponent } from './exportar-retencion/exportar-retencion.component';
import { DetalleRetencionComponent } from './detalle-retencion/detalle-retencion.component';
import { VistaRetencionesComponent } from './vista-retenciones/vista-retenciones.component';

@NgModule({
  declarations: [
    ListarRetencionesComponent,
    ExportarDatosComponent,
    EditarComponent,
    DejaSinEfectoComponent,
    ModificarComponent,
    MedioPagoComponent,
    DatoCobranteComponent,
    FormularioComponent,
    NuevaRetencionComponent,
    InicioNuevaRetencionComponent,
    DatosDemandadoComponent,
    DatosDemandanteComponent,
    IndicadoresRetencionComponent,
    InicioBuscarPersonaComponent,
    ExportarRetencionComponent,
    DetalleRetencionComponent,
    VistaRetencionesComponent
  ],
  imports: [
    CompartidoModule,
    MaterialModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: []
})
export class InicioModule { }
