import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiciosRetencionesJudicialesService } from '../../util/services/servicios-retenciones-judiciales.service';
import { saveAs } from 'file-saver';
import { DataTable } from 'primeng/components/datatable/datatable';
import { IndicadorLimiteUsarModificarMedioPago } from '../../util/interfaces/indicador-limite-usar-modificar-medio-pago';
import { RetencionJudicial } from '../../util/interfaces/retencion-judicial';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-listar-retenciones',
  templateUrl: './listar-retenciones.component.html',
  styleUrls: ['./listar-retenciones.component.css']
})
export class ListarRetencionesComponent implements OnInit {

  selectTipBus: any;
  imagenCargando: boolean = false;
  tipoBusqueda: any[];
  nombreDni: string = "";
  nombre: string;
  dni: string;
  apPat: string;
  apMat: string;
  prNom: string;
  sgNom: string;
  ngFechaInicial: Date = null;
  ngFechaFinal: Date = null;
  displayedColumns: string[] = ['Pro', 'Afectado', 'Secuencia de la Retención', 'CUSPP Demandado',
    'DNI Demandado', 'Nombre de Demandado', 'CUSPP Alimentista', 'Nombre de Alimentista', 'Fecha de Registro', 'Estado',
    'Editar', 'Ver'];
  dataSource: RetencionJudicial[] = [];

  totalRecords: number = 0;
  display: boolean = false;
  @ViewChild('dt') dataTable: DataTable;


  indiLimUsarMonFech: IndicadorLimiteUsarModificarMedioPago[];
  selectorIndLim: string;

  retencionJudicialEditar: RetencionJudicial = new RetencionJudicial();
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    private router: Router
  ) {
    this.inicializar();

  }

  ngOnInit() {
    this.displayedColumns;
    this.selectTipBus = this.tipoBusqueda[0];
    this.cargarDatos();
    this.agregarLista();
  }

  inicializar() {
    this.tipoBusqueda = [
      { id: 1, name: 'Nombre de Persona' },
      { id: 2, name: 'DNI' }
    ];
    this.nombre = "";
    this.dni = "";
  }

  cargarDatos() {
    this.imagenCargando = true;
    this._servicioRJ.getListarRetenciones(this.prNom, this.sgNom, this.apPat, this.apMat, this.dni, 0, false).subscribe(dato => {
      this.totalRecords = dato;
      this._servicioRJ.getListarRetenciones(this.prNom, this.sgNom, this.apPat, this.apMat, this.dni, 1, true).subscribe(dato => {
        this.dataSource = dato;
        this.imagenCargando = false;
      }, () => {
        this.imagenCargando = false;
      }, () => {
        this.imagenCargando = false;
      });
    });
  }

  editar(event: RetencionJudicial) {
    sessionStorage.removeItem('reteJudi');
    sessionStorage.setItem('reteJudi', JSON.stringify(event));
    this.retencionJudicialEditar = event;
    $("#editorModal").modal("show");
  }

  visualizar(event: RetencionJudicial) {
    sessionStorage.removeItem('reteJudi');
    sessionStorage.setItem('reteJudi', JSON.stringify(event));
    this.router.navigate(['detalle-retencion']);
  }

  loadCarsLazy(event: any) {
    this.imagenCargando = true;
    let paginaTabla = this.retornaPagina(event.first);
    this.cargarTabla(paginaTabla);
  }

  buscar() {
    this.cargarTabla();
  }

  limpiarCampos() {
    this.prNom = '';
    this.sgNom = '';
    this.apPat = '';
    this.apMat = '';
    this.dni = '';
  }

  cargarTabla(paginaTabla: number = 1) {
    this.dataSource = [];
    this.imagenCargando = true;
    this._servicioRJ.getListarRetenciones(this.prNom, this.sgNom, this.apPat, this.apMat, this.dni, 0, false).subscribe(dato => {
      this.totalRecords = dato;
      this._servicioRJ.getListarRetenciones(this.prNom, this.sgNom, this.apPat, this.apMat, this.dni, paginaTabla, true).subscribe(dato => {
        this.dataSource = dato;
        this.imagenCargando = false;
      });
    });
  }

  generarArchivo(fechaInicial: number, fechaFinal: number) {
    this.imagenCargando = true;
    this._servicioRJ.getDescargarRetencionesJudiciales(fechaInicial, fechaFinal, "").subscribe(dato => {
      var byteArray = new Uint8Array(dato);
      let today = new Date();
      let day = (today.getDate() < 10) ? "0" + today.getDate() : today.getDate();
      let month = ((today.getMonth() + 1) < 10) ? "0" + (today.getMonth() + 1) : (today.getMonth() + 1);
      let year = today.getFullYear();
      let nomDocumento: string = 'RETENCIONES_JUDICIALES_' + year + month + day + ".csv"
      saveAs(new Blob([byteArray], { type: '*/*' }), nomDocumento);
      this.imagenCargando = false;
    });
  }

  generarNuevaRetencion() {
    this.router.navigate(['nueva-retencion']);
  }

  retornaPagina(valor: number) {
    if (valor === 0) {
      return 1;
    } else {
      return (valor / 10) + 1;
    }
  }

  limpiarDatos() {
    this.ngFechaInicial = null;
    this.ngFechaFinal = null;
  }

  descargarArchivo(event: any[]) {
    if (event.length > 1) {
      this.generarArchivo(Number(event[0]), Number(event[1]));
    }
  }


  agregarLista() {
    this.indiLimUsarMonFech = [
      { valor: 'M', valorVer: 'Monto' },
      { valor: 'F', valorVer: 'Fecha' }
    ]
  }

}
