import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatoCobranteComponent } from './dato-cobrante.component';

describe('DatoCobranteComponent', () => {
  let component: DatoCobranteComponent;
  let fixture: ComponentFixture<DatoCobranteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatoCobranteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatoCobranteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
