import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DejaSinEfectoComponent } from './deja-sin-efecto.component';

describe('DejaSinEfectoComponent', () => {
  let component: DejaSinEfectoComponent;
  let fixture: ComponentFixture<DejaSinEfectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DejaSinEfectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DejaSinEfectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
