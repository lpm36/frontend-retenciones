import { Component, OnInit, Input } from '@angular/core';
import { RetencionJudicial } from '../../../util/interfaces/retencion-judicial';
declare var $: any;

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  modalEditar: String = '';

  @Input()
  retencionJudicialEditar: RetencionJudicial = new RetencionJudicial();

  retencionJudicialModif: RetencionJudicial = new RetencionJudicial();
  retencionJudicialSinEf: RetencionJudicial = new RetencionJudicial();
  constructor() {
  }

  ngOnInit() {
  }

  cargarDatos() {

  }

  modalModificar() {
    this.retencionJudicialModif = this.retencionJudicialEditar;
  }

  modalDejarSinEfecto() {
    this.retencionJudicialSinEf = this.retencionJudicialEditar;
  }

  recuperar(cadena: String) {
    this.modalEditar = cadena;
  }
}
