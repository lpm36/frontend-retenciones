import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { ActualizarDatoRetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/actualizar-datos-retencion';
import { Router } from '@angular/router';

declare var $: any;


@Component({
  selector: 'app-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.css']
})
export class ModificarComponent implements OnInit {

  @Input()
  retencionjudicial: RetencionJudicial;
  @Output() salidaModificacion = new EventEmitter();

  id_actual: number;
  imagenCargando: boolean = false;
  usuario = (JSON.parse(sessionStorage.getItem('usuario')) != null ? JSON.parse(sessionStorage.getItem('usuario')) : 'errUsu');
  actualizar: ActualizarDatoRetencionJudicial = new ActualizarDatoRetencionJudicial();
  mensajeError: string;

  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.id_actual = 1;
    this.cargarDatos();
  }

  cargarDatos() {
    //this.retencionjudicial = JSON.parse(sessionStorage.getItem('reteJudi'));
    sessionStorage.removeItem('reteJudi');
  }

  recuperar(retjud: RetencionJudicial) {
    this.retencionjudicial = retjud;
  }

  guardarCambios() {
    this.agregarDatos();
    $("#modificacionConfirmacionModificarModal").modal("hide");
    if (this.validarCampos()) {
      this.imagenCargando = true;
      this._servicioRJ.postActualizarRetencionJudicial(this.actualizar).subscribe(data => {
        this.imagenCargando = false;
        location.reload();
      });
    } else {
      $("#errorModal").modal("show");
    }
  }

  agregarDatos() {
    this.actualizar.indicadorLimUsar = this.retencionjudicial.indicadorHastaMontoFecha;
    this.actualizar.monedaRetencion = this.retencionjudicial.monedaRetencion;
    this.actualizar.monedaPado = this.retencionjudicial.monedaPago;
    this.actualizar.topeLimite = this.retencionjudicial.existeTopeLimite;
    this.actualizar.topeMontoPorsentaje = this.retencionjudicial.tipoTope;
    this.actualizar.porcentajeDescuento = this.retencionjudicial.porcentajeDescuento;
    this.actualizar.montoDescuento = this.retencionjudicial.montoDescuento;
    this.actualizar.montoTope = this.retencionjudicial.montoTope;
    this.actualizar.banco = this.retencionjudicial.banco;
    this.actualizar.nroCuentaDesposi = this.retencionjudicial.numCuentaDeposito;
    this.actualizar.formaPagoRetencion = this.retencionjudicial.formaPago;
    this.actualizar.ciaSeguroCargoReten = this.retencionjudicial.quienResponsablePago;
    this.actualizar.cartaRespuEnvJuzga = this.retencionjudicial.cartaRespuestaEnvJuz;
    this.actualizar.encargadoRetencion = this.retencionjudicial.encardoRetencion;
    this.actualizar.apePaterno = this.retencionjudicial.apePatCobrante;
    this.actualizar.apeMaterno = this.retencionjudicial.apeMatCobrante;
    this.actualizar.agencia = this.retencionjudicial.agenciaRecibeAtencion;
    this.actualizar.nroCheque = this.retencionjudicial.numCheque;

    this.actualizar.primerNombreCobrante = this.retencionjudicial.priNomCobrante;
    this.actualizar.segundNombreCobrante = this.retencionjudicial.segNomCobrante;
    this.actualizar.sexoCobrante = this.retencionjudicial.sexoCobrante;
    this.actualizar.tipoDocCobrante = this.retencionjudicial.tipDocCobrante;
    this.actualizar.nroDocCobrante = this.retencionjudicial.numDocCobrante;
    this.actualizar.cuspp = this.retencionjudicial.cusppCobrante;
    this.actualizar.sac = this.retencionjudicial.sacCobrante;
    this.actualizar.usuario = this.usuario;
    this.actualizar.tipoProceso = this.retencionjudicial.tipoProceso;
    this.actualizar.numIdent = this.retencionjudicial.numIdentificaion;
    this.actualizar.secuencia = this.retencionjudicial.secuencia;
  }

  validarCampos() {
    if (this.actualizar.porcentajeDescuento < 101) {
      if (this.actualizar.montoDescuento < 10000000000000) {
        if (this.actualizar.montoTope < 10000000000000) {
          return true;
        } else { this.mensajeError = 'El monto tope no puede ser mayor a 10000000000000'; }
      } else { this.mensajeError = 'El monto del descuento no puede ser mayor a 10000000000000'; }
    } else { this.mensajeError = 'El porcentaje de descuento no puede ser mayor a 100%'; }
    return false;
  }

  salidarModificacion() {
    this.salidaModificacion.emit('');
  }

}
