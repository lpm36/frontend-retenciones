import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IndicadorLimiteUsarModificarMedioPago } from 'projects/retencion-judicial/src/app/util/interfaces/indicador-limite-usar-modificar-medio-pago';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { TipoMoneda } from 'projects/retencion-judicial/src/app/util/interfaces/tipo-moneda';
import { GenericoCodValor } from 'projects/retencion-judicial/src/app/util/interfaces/generico-cod-valor';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { EntidadAseguradora } from 'projects/retencion-judicial/src/app/util/interfaces/entidad-aseguradora';
import { EntidadFinanciera } from 'projects/retencion-judicial/src/app/util/interfaces/entidad-financiera';
import { isNull, isNullOrUndefined } from 'util';
import { MedioPagoModificar } from 'projects/retencion-judicial/src/app/util/interfaces/medio-pago-modificar';
import { forkJoin } from 'rxjs';
import { ListaValor } from 'projects/retencion-judicial/src/app/util/interfaces/lista-valor';
import { RetencionAgencia } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-agencia';
import { BANCO_SCOTIABANK, AGENCIA_PRINCIPAL } from 'projects/retencion-judicial/src/app/util/compartido/constantes/constante-ruta-servicio-retencion-judicial';

@Component({
  selector: 'app-medio-pago',
  templateUrl: './medio-pago.component.html',
  styleUrls: ['./medio-pago.component.css']
})
export class MedioPagoComponent implements OnInit {

  @Input() retJud: RetencionJudicial;
  @Output() salidaMedioPago = new EventEmitter();
  formMedioPago: FormGroup;

  indiLimUsarMonFech: IndicadorLimiteUsarModificarMedioPago[];

  arrayMontoFecha: ListaValor[] = null;
  arrayMonedaRetencion: TipoMoneda[] = null;
  arrayMonedaPago: TipoMoneda[] = null;
  arrayExisteTipLim: ListaValor[] = null;
  arrayTopePorMontPorc: ListaValor[] = null;
  arrayFormaPago: ListaValor[] = null;
  arrayEncargadoRetencion: ListaValor[] = null;
  arrayEntidadAseguradora: EntidadAseguradora[] = null;
  arrayAgencias: RetencionAgencia[] = null;

  monedaRetencionList: TipoMoneda[];
  monedaPadoList: TipoMoneda[];
  topeLimiteList: GenericoCodValor[];
  topeMontoPorsentajeList: GenericoCodValor[];
  entidadAseguradoraList: EntidadAseguradora[];
  entidadFinancieraList: EntidadFinanciera[];
  imagenCargando: boolean = false;

  constructor(
    public _servicioRJ: ServiciosRetencionesJudicialesService,
    private fb: FormBuilder
  ) {
    this.cargaPreInicial();
  }

  ngOnInit() {
    this.cargaInicial();
    this.formulario();
    console.log('MedioPagoComponent');
  }
  cargaPreInicial() {
    this.agregarLista();
  }
  cargaInicial() {
    this.cargarMonedaRetencion();
  }

  agregarLista() {
    this.indiLimUsarMonFech = [
      { valor: 'M', valorVer: 'Monto' },
      { valor: 'F', valorVer: 'Fecha' }
    ];
    this.topeLimiteList = [
      { codigo: 'S', detalle: 'Si' },
      { codigo: 'N', detalle: 'No' }
    ];
    this.topeMontoPorsentajeList = [
      { codigo: 'M', detalle: 'Monto' },
      { codigo: 'P', detalle: 'Porcentaje' }
    ];
  }

  formulario() {
    this.formMedioPago = this.fb.group({
      indicadorLimUsar: [isNullOrUndefined(this.retJud.indicadorHastaMontoFecha) ? '' : this.retJud.indicadorHastaMontoFecha],
      monedaRetencion: [isNullOrUndefined(this.retJud.monedaRetencion) ? '' : this.retJud.monedaRetencion],
      monedaPado: [isNullOrUndefined(this.retJud.monedaPago) ? '' : this.retJud.monedaPago],
      topeLimite: [this.retJud.existeTopeLimite],
      topeMontoPorsentaje: [this.retJud.tipoTope],
      porcentajeDescuento: [this.retJud.porcentajeDescuento],
      montoDescuento: [this.retJud.montoDescuento],
      montoTope: [this.retJud.montoTope],
      banco: [this.retJud.banco],
      nroCuentaDesposi: [this.retJud.numCuentaDeposito],
      nroCheque: [this.retJud.numCheque],
      agencia: [this.retJud.agenciaRecibeAtencion],
      formaPagoRetencion: [this.retJud.formaPago],
      ciaSeguroCargoReten: [this.retJud.ciaSeguros],
      cartaRespuEnvJuzga: [this.retJud.cartaRespuestaEnvJuz],
      encargadoRetencion: [this.retJud.encardoRetencion]
    });
    this.formMedioPago.valueChanges.subscribe(data => {
      this.retJud.indicadorHastaMontoFecha = data.indicadorLimUsar;
      this.retJud.monedaRetencion = data.monedaRetencion;
      this.retJud.monedaPago = data.monedaPado;
      this.retJud.existeTopeLimite = data.topeLimite;
      this.retJud.tipoTope = data.topeMontoPorsentaje;
      this.retJud.porcentajeDescuento = data.porcentajeDescuento;
      this.retJud.montoDescuento = data.montoDescuento;
      this.retJud.montoTope = data.montoTope;
      this.retJud.banco = data.banco;
      this.retJud.numCuentaDeposito = data.nroCuentaDesposi;
      this.retJud.numCheque = data.nroCheque;
      this.retJud.formaPago = data.formaPagoRetencion;
      this.retJud.quienResponsablePago = data.ciaSeguroCargoReten;
      this.retJud.cartaRespuestaEnvJuz = data.cartaRespuEnvJuzga;
      this.retJud.encardoRetencion = data.encargadoRetencion;
      this.retJud.agenciaRecibeAtencion = data.agencia;
      this.cargarDatosFinales();
      this.salidaMedioPago.emit(this.retJud);
    });
  }

  cargarMonedaRetencion() {
    this.imagenCargando = true;
    this._servicioRJ.getTipoMonde().subscribe(data => {
      this.monedaRetencionList = data;
      this.monedaPadoList = data;
      this.cargarCia();
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarCia() {
    this.imagenCargando = true;
    this._servicioRJ.getEntidadAseguradoras().subscribe(data => {
      this.entidadAseguradoraList = data;
      this.cargarEntidadFinanciera();
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarEntidadFinanciera() {
    this.imagenCargando = true;
    this._servicioRJ.getEntidadFinanciearDeposito().subscribe(data => {
      this.entidadFinancieraList = data;
      this.cargarCombos();
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarDatosFinales() {
    switch (this.retJud.formaPago) {
      case 'J':
        this.retJud.banco = BANCO_SCOTIABANK;
        this.retJud.agenciaRecibeAtencion = AGENCIA_PRINCIPAL;
        this.retJud.numCheque = "";
        this.retJud.numCuentaDeposito = "";
        break;
      case 'C':
        this.retJud.numCuentaDeposito = "";
        break;
      case 'A':
        this.retJud.numCheque = "";
        this.retJud.agenciaRecibeAtencion = "";
        break;
      case 'V':
        this.retJud.numCheque = "";
        this.retJud.agenciaRecibeAtencion = "";
        this.retJud.numCuentaDeposito = "";
        break;
      default:
        break;
    }
  }

  cargarCombos() {
    const getMontFecha = this._servicioRJ.getListaMontoFecha();
    const getTipoMoneda = this._servicioRJ.getTipoMonde();
    const getExisteLimite = this._servicioRJ.getListaExisteLimite();
    const getMontoPorcensaje = this._servicioRJ.getListaMontoPorcentaje();
    const getForPago = this._servicioRJ.getListaFormaPago();
    const getEncargadoRet = this._servicioRJ.getListaEncargadoRetencion();
    const getEntidadesAseguradoras = this._servicioRJ.getEntidadAseguradoras();
    const getAgencia = this._servicioRJ.getListarAgencias();
    forkJoin([getMontFecha, getTipoMoneda, getExisteLimite, getMontoPorcensaje, getForPago, getEncargadoRet]).subscribe(data => {
      this.arrayMontoFecha = data[0];
      this.arrayMonedaRetencion = data[1];
      this.arrayMonedaPago = data[1];
      this.arrayExisteTipLim = data[2];
      this.arrayTopePorMontPorc = data[3];
      this.arrayFormaPago = data[4];
      this.arrayEncargadoRetencion = data[5];
      forkJoin([getEntidadesAseguradoras, getAgencia]).subscribe(rest => {
        this.arrayEntidadAseguradora = rest[0];
        this.arrayAgencias = rest[1];
        this.imagenCargando = false;
      }, () => {
        this.imagenCargando = false;
      });
    }, () => {
      this.imagenCargando = false;
    });

  }

  validar_enteros(t: any, n: any) {
    console.log(t, n)
  }
}
