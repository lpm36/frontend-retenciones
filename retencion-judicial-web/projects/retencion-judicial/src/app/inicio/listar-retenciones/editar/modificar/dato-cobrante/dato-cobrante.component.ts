import { Component, Input, OnChanges, Output, SimpleChanges, EventEmitter, OnInit } from '@angular/core';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { FormGroup, FormControl } from '@angular/forms';
import { DatoCobranteModificar } from 'projects/retencion-judicial/src/app/util/interfaces/dato-cobrante-modificar';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { TipoIdentificacion } from 'projects/retencion-judicial/src/app/util/interfaces/tipo-identificacion';
import { ListaValor } from 'projects/retencion-judicial/src/app/util/interfaces/lista-valor';



@Component({
  selector: 'app-dato-cobrante',
  templateUrl: './dato-cobrante.component.html',
  styleUrls: ['./dato-cobrante.component.css']
})
export class DatoCobranteComponent implements OnInit {


  @Input() retJudi: RetencionJudicial;
  @Output() salidaDatoCobrante = new EventEmitter();

  imagenCargando: boolean = false;
  tipoIdentificacionList: TipoIdentificacion[];
  sexoList: ListaValor[];
  form: FormGroup;
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
  ) {
  }

  ngOnInit() {
    this.cargarInicial();
    this.cargarFormulario();
  }

  cargarFormulario() {
    this.form = new FormGroup({
      apePaterno: new FormControl(this.retJudi.apePatCobrante),
      apeMaterno: new FormControl(this.retJudi.apeMatCobrante),
      primerNombreCobrante: new FormControl(this.retJudi.priNomCobrante),
      segundNombreCobrante: new FormControl(this.retJudi.segNomCobrante),
      sexoCobrante: new FormControl(this.retJudi.sexoCobrante),
      tipoDocCobrante: new FormControl(this.retJudi.tipDocCobrante),
      nroDocCobrante: new FormControl(this.retJudi.numDocCobrante),
      cuspp: new FormControl(this.retJudi.cusppCobrante),
      sac: new FormControl(this.retJudi.sacCobrante)
    });
    this.form.valueChanges.subscribe(data => {
      this.retJudi.apePatCobrante = data.apePaterno;
      this.retJudi.apeMatCobrante = data.apeMaterno;
      this.retJudi.priNomCobrante = data.primerNombreCobrante;
      this.retJudi.segNomCobrante = data.segundNombreCobrante;
      this.retJudi.sexoCobrante = data.sexoCobrante;
      this.retJudi.tipDocCobrante = data.tipoDocCobrante;
      this.retJudi.numDocCobrante = data.nroDocCobrante;
      this.retJudi.cusppCobrante = data.cuspp;
      this.retJudi.sacCobrante = data.sac;
      this.salidaDatoCobrante.emit(this.retJudi);
    });
  }

  cargarInicial() {
    this.cargarTipoIdentificacion();
    this.cargarSexo();
  }

  cargarTipoIdentificacion() {
    this.imagenCargando = true;
    this._servicioRJ.getTipoIdentificacion().subscribe(data => {
      this.tipoIdentificacionList = data;
      this.imagenCargando = false;
    }, error => {
      this.imagenCargando = false;
    });
  }

  cargarSexo() {
    this.imagenCargando = true;
    this._servicioRJ.getTipoSexo().subscribe(data => {
      this.sexoList = data;
      this.imagenCargando = false;
    }, error => {
      this.imagenCargando = false;
    });
  }

}
