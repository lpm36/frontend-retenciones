import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { DejarSinEfecto } from 'projects/retencion-judicial/src/app/util/interfaces/dejar-sin-efecto';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  @Input('retJud') retJud: RetencionJudicial;
  formDejarSinEfecto: FormGroup;
  @Output() salidaFormulario = new EventEmitter();


  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.cargaInicial();

  }

  cargaInicial() {
    this.cargarFormulario();
  }

  cargarFormulario() {
    this.formDejarSinEfecto = this.fb.group({
      numOficio: [this.retJud.numOficio],
      numExpediente: [''],
      numSac: [this.retJud.numeroSacReclamo],
      nombreJuzgado: [this.retJud.juzgado],
      observacionSinEfecto: [this.retJud.comentario]
    });
    this.formDejarSinEfecto.valueChanges.subscribe(data => {
      this.cargarDatos(data.numOficio, data.numExpediente, data.numSac,
        data.nombreJuzgado, data.observacionSinEfecto);
      this.salidaFormulario.emit(this.retJud);
    });
  }
  cargarDatos(numOficio: string, numExpediente: string, numSac: string,
    nombreJuzgado: string, observacionSinEfecto: string) {
    this.retJud.numOficio = numOficio;
    this.retJud.numeroSacReclamo = numSac;
    this.retJud.juzgado = nombreJuzgado;
    this.retJud.comentario = observacionSinEfecto;
  }

}
