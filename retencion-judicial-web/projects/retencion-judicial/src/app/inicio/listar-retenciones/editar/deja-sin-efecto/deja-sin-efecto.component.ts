import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { DejarSinEfecto } from 'projects/retencion-judicial/src/app/util/interfaces/dejar-sin-efecto';
import { TITULO_DE_CONFIRMACION_GENERICO, MENSAJE_DE_CONFIRMACION_GENERICO, TITULO_DE_ERROR_GENERICO, MENSAJE_DE_ERROR_GENERICO } from 'projects/retencion-judicial/src/app/util/compartido/constantes/constante-ruta-servicio-retencion-judicial';

declare var $: any;

@Component({
  selector: 'app-deja-sin-efecto',
  templateUrl: './deja-sin-efecto.component.html',
  styleUrls: ['./deja-sin-efecto.component.css']
})
export class DejaSinEfectoComponent implements OnInit {

  @Input() retencionjudicial: RetencionJudicial;
  @Output() salidaDejarSinEfecto = new EventEmitter();

  titutoModal: string;
  mensajeModal: string;
  imagenCargando: boolean = false;
  dejarSinEfecto: DejarSinEfecto = new DejarSinEfecto();
  usuario = (JSON.parse(sessionStorage.getItem('usuario')) != null ? JSON.parse(sessionStorage.getItem('usuario')) : 'errUsu');
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService
  ) {

  }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.retencionjudicial = JSON.parse(sessionStorage.getItem('reteJudi'));
    sessionStorage.removeItem('reteJudi');
  }

  recuperar(retJud: RetencionJudicial) {
    this.retencionjudicial = retJud;
  }

  cambiarDejarSinEfecto() {
    this.cargarDatosDejarSinEfecto();
    this.imagenCargando = true;
    this._servicioRJ.postDejarSinEfecto(this.dejarSinEfecto).subscribe(data => {
      this.imagenCargando = false;
      if (data > 0) {
        this.titutoModal = TITULO_DE_CONFIRMACION_GENERICO;
        this.mensajeModal = MENSAJE_DE_CONFIRMACION_GENERICO;
        $("#dejarSinEfectoInformativoModal").modal("show");
      } else {
        this.titutoModal = TITULO_DE_ERROR_GENERICO;
        this.mensajeModal = MENSAJE_DE_ERROR_GENERICO;
        $("#dejarSinEfectoInformativoModal").modal("show");
      }
    }, () => {
      this.imagenCargando = false;
      this.titutoModal = TITULO_DE_ERROR_GENERICO;
      this.mensajeModal = MENSAJE_DE_ERROR_GENERICO;
      $("#dejarSinEfectoInformativoModal").modal("show");
    });
  }

  cargarDatosDejarSinEfecto() {
    this.dejarSinEfecto.numOficio = this.retencionjudicial.numOficio;
    this.dejarSinEfecto.numExpediente = '';
    this.dejarSinEfecto.numSac = this.retencionjudicial.numeroSacReclamo;
    this.dejarSinEfecto.nombreJuzgado = this.retencionjudicial.juzgado;
    this.dejarSinEfecto.observacion = this.retencionjudicial.comentario;
    this.dejarSinEfecto.usuario = this.usuario;
    this.dejarSinEfecto.tipoProceso = this.retencionjudicial.tipoProceso;
    this.dejarSinEfecto.numIdent = this.retencionjudicial.numIdentificaion;
    this.dejarSinEfecto.secuencia = this.retencionjudicial.secuencia;
  }

  salidarDejarSinEfecto() {
    this.salidaDejarSinEfecto.emit('');
  }
}
