import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarRetencionesComponent } from './listar-retenciones.component';

describe('ListarRetencionesComponent', () => {
  let component: ListarRetencionesComponent;
  let fixture: ComponentFixture<ListarRetencionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarRetencionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarRetencionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
