import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { forkJoin } from 'rxjs';
import { TipoMoneda } from 'projects/retencion-judicial/src/app/util/interfaces/tipo-moneda';
import { EntidadFinanciera } from 'projects/retencion-judicial/src/app/util/interfaces/entidad-financiera';
import { EntidadAseguradora } from 'projects/retencion-judicial/src/app/util/interfaces/entidad-aseguradora';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { RetigistroRetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retigistro-retencion-judicial';
import { RetencionAgencia } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-agencia';
import { ListaValor } from 'projects/retencion-judicial/src/app/util/interfaces/lista-valor';
import { AGENCIA_PRINCIPAL, BANCO_SCOTIABANK } from 'projects/retencion-judicial/src/app/util/compartido/constantes/constante-ruta-servicio-retencion-judicial';
declare var $: any;

@Component({
  selector: 'app-indicadores-retencion',
  templateUrl: './indicadores-retencion.component.html',
  styleUrls: ['./indicadores-retencion.component.css']
})
export class IndicadoresRetencionComponent implements OnInit {

  @Input('retigistroIndicadores') retRetJud: RetigistroRetencionJudicial;
  @Output() retornarRetJudDatosIndicadores = new EventEmitter<RetigistroRetencionJudicial>();

  maxDate: Date;
  minDate: Date;
  yearRange: string;
  es: any;
  fechaActual: string;
  ngFechaDesde: Date;
  arrayMonedaRetencion: TipoMoneda[] = null;
  arrayMonedaPago: TipoMoneda[] = null;
  arrayMontoFecha: ListaValor[] = null;
  arrayEncargadoRetencion: ListaValor[] = null;
  arrayExisteTipLim: ListaValor[] = null;
  arrayTopePorMontPorc: ListaValor[] = null;
  arrayFormaPago: ListaValor[] = null;
  arrayEnviarConsultaJuzgado: ListaValor[] = null;
  arrayEntidadFinanciera: EntidadFinanciera[] = null;
  arrayEntidadAseguradora: EntidadAseguradora[] = null;
  arrayAgencias: RetencionAgencia[] = null;
  arrayIndicadorTipoProceso: ListaValor[] = null;
  arrayTipoAfectacion: ListaValor[] = null;
  formIndicadoresFormularioFormaPago: FormGroup;
  formIndicadoresFormularioEncargadoRetencion: FormGroup;
  formIndicadoresFormularioDatosGeneral: FormGroup;
  formIndicadoresFormulario: FormGroup;
  imagenCargando: boolean = false;
  arrayEntidadFinancieraBanco: EntidadFinanciera[] = null;
  arrayEntidadFinancieraChequeConsignacionJudicial: EntidadFinanciera[] = null;
  arrayEntidadFinancieraVentanilla: EntidadFinanciera[] = null;
  agenciasDisable: boolean = false;
  bancosDisable: boolean = false;
  numCuentaDeposDesable: boolean = false;
  numChequeDesable: boolean = false;
  activarConJud: boolean = false;
  tituloMensaje: string;
  textoMensaje: string;
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    private fbIndicadores: FormBuilder,
    private fbIndicadoresPago: FormBuilder,
    private fbIndicadoresEncargadoRetencion: FormBuilder,
    private fbIndicadoresDatosGeneral: FormBuilder
  ) {
    this.fechaMaxMin();
  }

  ngOnInit() {
    this.cargarFormularios();
    this.cargarInicial();
    this.cargarComboEntidadFinanciera();
  }

  cargarInicial() {
    this.combosManual();
    this.cargarFecha();
    this.cargarMesesEsp();
    this.cargarCombos();
  }

  cargarFormularios() {
    this.cargarIndicadoresFormulario();
    this.cargarFormularioPago();
    this.cargarFormularioRetencion();
    this.cargarFormularioDatosGenral();
  }

  cargarFormularioPago() {
    this.formIndicadoresFormularioFormaPago = this.fbIndicadoresPago.group({
      formaPago: [(this.retRetJud.formaPago != null ? this.retRetJud.formaPago : '')],
      banco: [(this.retRetJud.banco != null ? this.retRetJud.banco : '')],
      agencia: [(this.retRetJud.agencia != null ? this.retRetJud.agencia : '')],
      numCuentaDepos: [(this.retRetJud.numCuentaDepo != null ? this.retRetJud.numCuentaDepo : '')],
      numCheque: [(this.retRetJud.numCheque != null ? this.retRetJud.numCheque : '')]
    });
    this.formIndicadoresFormularioFormaPago.valueChanges.subscribe(data => {
      this.retRetJud.formaPago = data.formaPago;
      this.retRetJud.banco = data.banco;
      this.retRetJud.agencia = data.agencia;
      this.retRetJud.numCuentaDepo = data.numCuentaDepos;
      this.retRetJud.numCheque = data.numCheque;
      this.cargarComboEntidadFinanciera();
      this.limpiarNumeroCuentaDeposito(data.numCuentaDepos, data.formaPago);
      this.limpiarNumeroCheque(data.numCheque, data.formaPago);
      this.retornarRetJudDatosIndicadores.emit(this.retRetJud);
    });
  }

  cargarFormularioRetencion() {
    this.formIndicadoresFormularioEncargadoRetencion = this.fbIndicadoresEncargadoRetencion.group({
      encargadoRetencion: [(this.retRetJud.encargadoRetencion != null ? this.retRetJud.encargadoRetencion : '')],
      ciaEncargadoRetencion: [(this.retRetJud.ciaRetencion != null ? this.retRetJud.ciaRetencion : '')],
      cartaRespuestaEnvJuzgado: [(this.retRetJud.cartaRespuestaEnviJuzga != null ? this.retRetJud.cartaRespuestaEnviJuzga : '')]
    });
    this.formIndicadoresFormularioEncargadoRetencion.valueChanges.subscribe(data => {
      this.retRetJud.encargadoRetencion = data.encargadoRetencion;
      this.retRetJud.ciaRetencion = data.ciaEncargadoRetencion;
      this.retRetJud.cartaRespuestaEnviJuzga = data.cartaRespuestaEnvJuzgado;
      this.retornarRetJudDatosIndicadores.emit(this.retRetJud);
    });
  }

  cargarFormularioDatosGenral() {
    this.formIndicadoresFormularioDatosGeneral = this.fbIndicadoresDatosGeneral.group({
      fchRecepcionOficio: [(this.retRetJud.fchRecepOficio != null && this.retRetJud.fchRecepOficio != undefined ? this.devolverformatoFecha(this.retRetJud.fchRecepOficio) : '')],
      seEnvConsultaJuzgado: [(this.retRetJud.seEnviaConsuldoJuzgado != null ? this.retRetJud.seEnviaConsuldoJuzgado : '')],
      numSacReclamo: [(this.retRetJud.numSacReclamo != null ? this.retRetJud.numSacReclamo : '')],
      fchOficio: [(this.retRetJud.fchOficio != null ? this.devolverformatoFecha(this.retRetJud.fchOficio) : '')],
      indTipoProceso: [(this.retRetJud.tipoProceso != null ? this.retRetJud.tipoProceso : '')],
      tipoAfectacion: [(this.retRetJud.tipoAfectacion != null ? this.retRetJud.tipoAfectacion : '')],
      nombreJuez: [(this.retRetJud.nombJuez != null ? this.retRetJud.nombJuez : '')],
      fchEmisionCarta: [(this.retRetJud.fchEmisionCarta != null ? this.devolverformatoFecha(this.retRetJud.fchEmisionCarta) : '')],
      corte: [(this.retRetJud.corte != null ? this.retRetJud.corte : '')],
      observacion: [(this.retRetJud.observacion != null ? this.retRetJud.observacion : '')]
    });
    this.formIndicadoresFormularioDatosGeneral.valueChanges.subscribe(data => {
      this.retRetJud.fchRecepOficio = this.transformarFechaAs(new Date(data.fchRecepcionOficio).toLocaleDateString());
      this.retRetJud.seEnviaConsuldoJuzgado = data.seEnvConsultaJuzgado;
      this.retRetJud.numSacReclamo = data.numSacReclamo;
      this.retRetJud.fchOficio = this.transformarFechaAs(new Date(data.fchOficio).toLocaleDateString());
      this.retRetJud.tipoProceso = data.indTipoProceso;
      this.retRetJud.fchEmisionCarta = this.transformarFechaAs(new Date(data.fchEmisionCarta).toLocaleDateString());
      this.retRetJud.corte = data.corte;
      this.retRetJud.observacion = data.observacion
      this.retRetJud.nombJuez = data.nombreJuez;
      this.retRetJud.tipoAfectacion = data.tipoAfectacion;
      this.retornarRetJudDatosIndicadores.emit(this.retRetJud);
    });
  }

  cargarIndicadoresFormulario() {
    this.formIndicadoresFormulario = this.fbIndicadores.group({
      indicadorLimiteMontFech: [(this.retRetJud.indicadorLimUsarMontFch != null ? this.retRetJud.indicadorLimUsarMontFch : '')],
      monedaRetencion: [(this.retRetJud.monedaRetencio != null ? this.retRetJud.monedaRetencio : '')],
      monedaPago: [(this.retRetJud.monedaRetencio != null ? this.retRetJud.monedaRetencio : '')],
      fechaInicio: [(this.retRetJud.fchInicioIndicador != null ? this.devolverformatoFecha(this.retRetJud.fchInicioIndicador) : '')],
      fechaFin: [(this.retRetJud.fchFinIndicador != null ? this.devolverformatoFecha(this.retRetJud.fchFinIndicador) : '')],
      existeTopeLimit: [(this.retRetJud.exitMontoLimit != null ? this.retRetJud.exitMontoLimit : '')],
      topeMontoPorcentaje: [(this.retRetJud.topeEsMontoPorcen != null ? this.retRetJud.topeEsMontoPorcen : '')],
      porcentajeDescuento: [(this.retRetJud.porcentajeDescuento != null ? this.retRetJud.porcentajeDescuento : '')],
      montoDescuento: [(this.retRetJud.montoDescuento != null ? this.retRetJud.montoDescuento : '')],
      montoTope: [(this.retRetJud.montoTope != null ? this.retRetJud.montoTope : '')]
    });
    this.formIndicadoresFormulario.valueChanges.subscribe(data => {
      this.retRetJud.indicadorLimUsarMontFch = data.indicadorLimiteMontFech;
      this.retRetJud.fchInicioIndicador = (data.fechaInicio != '' ? this.transformarFechaAs(new Date(data.fechaInicio).toLocaleDateString()) : '');
      this.retRetJud.fchFinIndicador = (data.fechaFin != '' ? this.transformarFechaAs(new Date(data.fechaFin).toLocaleDateString()) : '');
      this.retRetJud.monedaRetencio = data.monedaRetencion;
      this.retRetJud.monedaPago = data.monedaPago;
      this.retRetJud.exitMontoLimit = data.existeTopeLimit;
      this.retRetJud.topeEsMontoPorcen = data.topeMontoPorcentaje;
      this.retRetJud.porcentajeDescuento = data.porcentajeDescuento;
      this.retRetJud.montoDescuento = data.montoDescuento;
      this.retRetJud.montoTope = data.montoTope;
      this.limpiarTopeLimite(data.montoTope);
      this.limpiarMontoPorcentaje(data.montoDescuento, data.porcentajeDescuento);
      this.retornarRetJudDatosIndicadores.emit(this.retRetJud);
    });
  }
  limpiarTopeLimite(monto: number) {
    if (this.retRetJud.exitMontoLimit == 'N') {
      this.retRetJud.montoTope = 0;
      if (monto > 0) {
        this.formIndicadoresFormulario.controls['montoTope'].setValue(0);
      }
    }
  }

  limpiarMontoPorcentaje(monto: number, porcentaje: number) {
    switch (this.retRetJud.topeEsMontoPorcen) {
      case 'M':
        this.retRetJud.porcentajeDescuento = 0;
        if (porcentaje > 0) {
          this.formIndicadoresFormulario.controls['porcentajeDescuento'].setValue(0);
        }
        break;
      case 'P':
        this.retRetJud.montoDescuento = 0;
        if (monto > 0) {
          this.formIndicadoresFormulario.controls['montoDescuento'].setValue(0);
        }
        break;
      default:
        break;
    }
  }

  cargarFecha() {
    this.yearRange = "1994" + ":" + (new Date().getFullYear().toString());
  }

  fechaMaxMin() {
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let nextMonth = (month === 11) ? 0 : month;
    let nextYear = (nextMonth === 0) ? year : year;
    let prevMonth = (month === 0) ? 11 : month;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    this.fechaActual = this.validarMes(today.getDate(), month, year);
    this.maxDate = new Date();
    this.maxDate.setMonth(nextMonth);
    this.maxDate.setFullYear(nextYear);
    this.minDate = new Date();
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);
  }

  combosManual() {
    const getMontFecha = this._servicioRJ.getListaMontoFecha();
    const getExisteLimite = this._servicioRJ.getListaExisteLimite();
    const getMontoPorcensaje = this._servicioRJ.getListaMontoPorcentaje();
    const getForPago = this._servicioRJ.getListaFormaPago();
    forkJoin([getMontFecha, getExisteLimite, getMontoPorcensaje, getForPago]).subscribe(result => {
      this.arrayMontoFecha = result[0];
      this.arrayExisteTipLim = result[1];
      this.arrayTopePorMontPorc = result[2];
      this.arrayFormaPago = result[3];
    });
    const getEncargadoRet = this._servicioRJ.getListaEncargadoRetencion();
    const getEnviarConsta = this._servicioRJ.getListaEnviarJuzgado();
    forkJoin([getEncargadoRet, getEnviarConsta]).subscribe(datos => {
      this.arrayEncargadoRetencion = datos[0];
      this.arrayEnviarConsultaJuzgado = datos[1];
    });
  }

  cargarCombos() {
    this.imagenCargando = true;
    const getTipoMoneda = this._servicioRJ.getTipoMonde();
    const getEntidadesAseguradoras = this._servicioRJ.getEntidadAseguradoras();
    const getAgencia = this._servicioRJ.getListarAgencias();
    const getIndicadorTipoProceso = this._servicioRJ.getListaIndicadorTipoProceso();
    const getTipoAfectacinon = this._servicioRJ.getListaTipoAfectacion();
    const getVentanilla = this._servicioRJ.getEntidadFinanciearVentanilla();
    const getChequeConsignacionJudicial = this._servicioRJ.getEntidadFinanciearChequeConsignacionJudicial();
    const getDeposito = this._servicioRJ.getEntidadFinanciearDeposito();
    forkJoin([getTipoMoneda, getEntidadesAseguradoras, getAgencia, getIndicadorTipoProceso, getTipoAfectacinon]).subscribe(resultado => {
      this.imagenCargando = false;
      this.arrayMonedaPago = resultado[0];
      this.arrayMonedaRetencion = resultado[0];
      this.arrayEntidadAseguradora = resultado[1];
      this.arrayAgencias = resultado[2];
      this.arrayIndicadorTipoProceso = resultado[3];
      this.arrayTipoAfectacion = resultado[4];
    }, () => {
      this.imagenCargando = false;
    });
    forkJoin([getVentanilla, getChequeConsignacionJudicial, getDeposito]).subscribe(resultado => {
      this.imagenCargando = false;
      this.arrayEntidadFinancieraBanco = resultado[2];
      this.arrayEntidadFinancieraChequeConsignacionJudicial = resultado[1];
      this.arrayEntidadFinancieraVentanilla = resultado[0];
      this.cargarComboEntidadFinanciera();
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarComboEntidadFinanciera() {
    this.arrayEntidadFinanciera = null;
    switch (this.retRetJud.formaPago) {
      case 'V':
        this.agenciasDisable = false;
        this.bancosDisable = true;
        this.numCuentaDeposDesable = false;
        this.numChequeDesable = false;
        this.activarConJud = false;
        this.arrayEntidadFinanciera = this.arrayEntidadFinancieraVentanilla;
        break;
      case 'C':
        this.agenciasDisable = true;
        this.bancosDisable = true;
        this.numCuentaDeposDesable = false;
        this.numChequeDesable = true;
        this.activarConJud = false;
        this.arrayEntidadFinanciera = this.arrayEntidadFinancieraChequeConsignacionJudicial;
        break;
      case 'J':
        this.agenciasDisable = false;
        this.bancosDisable = false;
        this.numCuentaDeposDesable = false;
        this.numChequeDesable = false;
        this.activarConJud = true;
        this.cargarDatosConsignacionJudicial();
        this.arrayEntidadFinanciera = this.arrayEntidadFinancieraChequeConsignacionJudicial;
        break;
      case 'A':
        this.agenciasDisable = false;
        this.bancosDisable = true;
        this.numCuentaDeposDesable = true;
        this.numChequeDesable = false;
        this.activarConJud = false;
        this.arrayEntidadFinanciera = this.arrayEntidadFinancieraBanco;
        break;
      default:
        break;
    }
  }


  limpiarNumeroCuentaDeposito(numCuenta: string, formaPago: string) {
    if (numCuenta != '' && formaPago != 'A') {
      this.retRetJud.numCuentaDepo = '';
      this.formIndicadoresFormularioFormaPago.controls['numCuentaDepos'].setValue('');
    }
  }

  limpiarNumeroCheque(numeCheque: string, formaPago: string) {
    if (numeCheque != '' && formaPago != 'C') {
      this.retRetJud.numCheque = '';
      this.formIndicadoresFormularioFormaPago.controls['numCheque'].setValue('');
    }
  }

  validarMes(dia: number, mes: number, anio: number) {
    let mesF = (mes < 10 ? '0' + mes.toString() : mes.toString());
    return dia.toString() + '/' + mesF + '/' + anio.toString();
  }

  devolverformatoFecha(fecha: string) {
    if (fecha.length > 0) {
      let anio: string = fecha.charAt(0) + fecha.charAt(1) + fecha.charAt(2) + fecha.charAt(3);
      let mes: string = fecha.charAt(4) + fecha.charAt(5);
      let dia: string = fecha.charAt(6) + fecha.charAt(7);
      return dia.concat('/').concat(mes).concat('/').concat(anio);
    } else {
      return '';
    }
  }

  transformarFechaAs(fecha: string) {
    if (fecha.length > 0) {
      let split = fecha.split('/');
      return split[2] + (parseInt(split[1]) < 10 ? '0' + split[1] : split[1]) + (parseInt(split[0]) < 10 ? '0' + split[0] : split[0]);
    } else {
      return '';
    }
  }

  cargarDatosConsignacionJudicial() {
    this.retRetJud.banco = BANCO_SCOTIABANK;
    this.retRetJud.agencia = AGENCIA_PRINCIPAL;
    this.tituloMensaje = 'Información';
    this.textoMensaje = 'El cobrante al guardar, sera registrado como BANCO DE LA NACION';
    this.retRetJud.apeMatCobrante = "RUC 20100030595";
    this.retRetJud.apePatCobrante = "BANCO DE LA NACION";
    this.retRetJud.priNomCobrante = "";
    this.retRetJud.segNomCobrante = "";
    this.retRetJud.sacCobrante = "";
    this.retRetJud.cusppCobrante = "";
    this.retRetJud.sexoCobrante = "M";
    this.retRetJud.tipoDocCobrante = "";
    this.retRetJud.numDocCobrante = "";
    $("#indicadoresRetencionModal").modal("show");
  }

  cargarMesesEsp() {
    this.es = {
      firstDayOfWeek: 1,
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      today: 'Hoy',
      clear: 'Borrar'
    }
  }

}
