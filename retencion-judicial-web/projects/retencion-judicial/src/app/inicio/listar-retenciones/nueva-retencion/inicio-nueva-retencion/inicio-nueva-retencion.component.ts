import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { ListaValor } from 'projects/retencion-judicial/src/app/util/interfaces/lista-valor';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { TIPO_DE_PROCESO } from 'projects/retencion-judicial/src/app/util/compartido/constantes/constante-ruta-servicio-retencion-judicial';
import { RetencionAfiliado } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-afiliado';
import { RetencionBeneficiario } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-beneficiario';
import { RetigistroRetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retigistro-retencion-judicial';
declare var $: any;


@Component({
  selector: 'app-inicio-nueva-retencion',
  templateUrl: './inicio-nueva-retencion.component.html',
  styleUrls: ['./inicio-nueva-retencion.component.css']
})
export class InicioNuevaRetencionComponent implements OnInit {


  @Output() retornarRetJudNuevReten = new EventEmitter<RetigistroRetencionJudicial>();
  @Input('retigistroRetencionJudicial') retRetJud: RetigistroRetencionJudicial;
  tipoProcesoList: ListaValor[];
  seAfectaList: any[];
  imagenCargando: boolean = false;
  selectedValueTipoProceso: string = TIPO_DE_PROCESO;
  selectedValueAfectado: string = '';
  formularioInicioNuevaRetencion: FormGroup;
  dniDemandado: string;
  cusppDemandado: string;
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    public fb: FormBuilder
  ) { }

  ngOnInit() {
    this.cargarInicial();
  }

  cargarInicial() {
    this.cargarTipoProceso();
    this.cargarSeAfecta();
    this.cargarDatos();
  }

  cargarDatos() {
    this.dniDemandado = this.retRetJud.numDocDemandado;
    this.cusppDemandado = this.retRetJud.cusppDemandado;
    this.selectedValueAfectado = this.retRetJud.afectaDemandado;
  }

  cargarTipoProceso() {
    this.imagenCargando = true;
    this._servicioRJ.getTipoProceso().subscribe(data => {
      this.tipoProcesoList = data;
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarSeAfecta() {
    this.seAfectaList = [{ id: 'A', value: 'Afiliado' }, { id: 'B', value: 'Beneficiario' }];
  }

  cargarFormulario() {
    this.formularioInicioNuevaRetencion = this.fb.group({
      topeLimite: ['']
    });
    this.formularioInicioNuevaRetencion.valueChanges.subscribe(data => {
    });
  }

  buscarPersona() {
    $("#inicioBuscarPersonaModal").modal({ backdrop: "static" });
  }

  personaSelecccionadoAfliado(objet: RetencionAfiliado) {
    if (objet.cuspp != null || objet.cuspp != undefined) {
      this.demandadoAfilaido(objet);
    }
    $("#inicioBuscarPersonaModal").modal("hide");
  }

  personaSelecccionadoBeneficiario(objet: RetencionBeneficiario) {
    if (objet.cuspp != null || objet.cuspp != undefined) {
      this.demandadoBeneficiario(objet);
    }
    $("#inicioBuscarPersonaModal").modal("hide");
  }

  demandadoAfilaido(objet: RetencionAfiliado) {

    this.retRetJud.cusppDemandado = objet.cuspp;
    this.retRetJud.numDocDemandado = objet.numDocumento;
    this.retRetJud.afectaDemandado = 'A';
    this.retRetJud.apePatDemandado = objet.primerApe;
    this.retRetJud.apeMatDemandado = objet.segundoApe;
    this.retRetJud.priNomDemandado = objet.primerNom;
    this.retRetJud.segNomDemandado = objet.segundoNom;
    this.retRetJud.sexoDemandado = objet.sexo;
    this.retRetJud.tipoDocDemandado = 'DNI';
    this.retRetJud.numDocDemandado = objet.numDocumento;
    this.selectedValueAfectado = 'A';
    this.dniDemandado = objet.numDocumento;
    this.cusppDemandado = objet.cuspp;
    this.retornarRetJudNuevReten.emit(this.retRetJud);
  }

  demandadoBeneficiario(objet: RetencionBeneficiario) {
    this.retRetJud.cusppDemandado = objet.cuspp;
    this.retRetJud.numDocDemandado = objet.numDocumento;
    this.retRetJud.afectaDemandado = 'B';
    this.retRetJud.apePatDemandado = objet.primerApe;
    this.retRetJud.apeMatDemandado = objet.segundoApe;
    this.retRetJud.priNomDemandado = objet.primerNom;
    this.retRetJud.segNomDemandado = objet.segundoNom;
    this.retRetJud.sexoDemandado = objet.sexo;
    this.retRetJud.numBeneficiarioDemandando = objet.tipo;
    this.retRetJud.descripNumBeneficiarioDemandando = objet.descTipo;
    this.retRetJud.secBeneficiarioDemandado = objet.numBeneficiario;
    this.retRetJud.tipoDocDemandado = 'DNI';
    this.retRetJud.numDocDemandado = objet.numDocumento;
    this.selectedValueAfectado = 'B';
    this.dniDemandado = objet.numDocumento;
    this.cusppDemandado = objet.cuspp;
    sessionStorage.removeItem('tipoBeneficiario')
    sessionStorage.removeItem('numBeneficiario')
    sessionStorage.setItem('tipoBeneficiario', objet.tipo);
    sessionStorage.setItem('numBeneficiario', objet.numBeneficiario);
    this.retornarRetJudNuevReten.emit(this.retRetJud);
  }
}
