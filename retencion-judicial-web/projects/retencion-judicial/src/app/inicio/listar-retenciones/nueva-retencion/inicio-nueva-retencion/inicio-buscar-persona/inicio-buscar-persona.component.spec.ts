import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioBuscarPersonaComponent } from './inicio-buscar-persona.component';

describe('InicioBuscarPersonaComponent', () => {
  let component: InicioBuscarPersonaComponent;
  let fixture: ComponentFixture<InicioBuscarPersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioBuscarPersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioBuscarPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
