import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadoresRetencionComponent } from './indicadores-retencion.component';

describe('IndicadoresRetencionComponent', () => {
  let component: IndicadoresRetencionComponent;
  let fixture: ComponentFixture<IndicadoresRetencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadoresRetencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadoresRetencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
