import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RetigistroRetencionJudicial } from '../../../util/interfaces/retigistro-retencion-judicial';
import { ServiciosRetencionesJudicialesService } from '../../../util/services/servicios-retenciones-judiciales.service';
declare var $: any;

@Component({
  selector: 'app-nueva-retencion',
  templateUrl: './nueva-retencion.component.html',
  styleUrls: ['./nueva-retencion.component.css']
})
export class NuevaRetencionComponent implements OnInit {
  retigistroRetencionJudicial: RetigistroRetencionJudicial = new RetigistroRetencionJudicial();
  id_actual: number = 1;
  pasos: any[];
  mensajeError: string = '';
  mensajeInformativo: string = '';
  mensajeConfirmacion: string;
  imagenCargando: boolean = false;
  listaTipoProceso: any;
  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cargarPasos();
  }

  siguiente(id: any) {
    if (this.validarNavegacion()) {
      this.id_actual = this.id_actual + id;
    } else {
      $("#mensajeInformativoInformativoModal").modal("show");
    }

  }

  validarNavegacion() {
    switch (this.id_actual) {
      case 1:
        this.mensajeInformativo = 'Debe de buscar y selecciona una persona';
        return (this.retigistroRetencionJudicial.cusppDemandado != null ? true : false);
      case 3:
        this.mensajeInformativo = 'Debe de ingresar los datos del alimentista';
        return (
          this.retigistroRetencionJudicial.apeMatAlimentista != null && this.retigistroRetencionJudicial.apeMatAlimentista != '' &&
            this.retigistroRetencionJudicial.apePatAlimentista != null && this.retigistroRetencionJudicial.apePatAlimentista != '' &&
            this.retigistroRetencionJudicial.priNomAlimentista != null && this.retigistroRetencionJudicial.priNomAlimentista != '' &&
            this.retigistroRetencionJudicial.segNomAlimentista != null && this.retigistroRetencionJudicial.segNomAlimentista != '' &&
            this.retigistroRetencionJudicial.tipoDocAlimentista != null && this.retigistroRetencionJudicial.tipoDocAlimentista != '' &&
            this.retigistroRetencionJudicial.numDocAlimentista != null && this.retigistroRetencionJudicial.numDocAlimentista != ''
            ? true : false
        );
      default:
        return true;
    }
  }

  cancelar() {
    this.router.navigate(['listar-retenciones']);
  }

  obtenerRetJudi(retJud: RetigistroRetencionJudicial) {
    this.retigistroRetencionJudicial = retJud;
  }

  generarRetencion() {
    this.imagenCargando = true;
    $("#confirmacionNuevaRetencionModal").modal("hide");
    this.modificarTipoProcesoEnviar();
    this._servicioRJ.postNuevaRetencion(this.retigistroRetencionJudicial).subscribe(dato => {
      if (dato == 1) {
        this.mensajeConfirmacion = 'El Proceso se realizo exitosamente';
        $("#validacionconfirmacionNuevaRetencionModal").modal({ backdrop: "static" });
        this.imagenCargando = false;
      } else {
        this.mensajeError = 'No se pudo registrar';
        $("#mensajeInformativoErrorModal").modal("show");
        this.retigistroRetencionJudicial.tipoProceso = this.listaTipoProceso;
        this.imagenCargando = false;
      }
      this.imagenCargando = false;
    }, () => {
      this.mensajeError = 'Ocurrio un problema al realizar el proceso.';
      $("#mensajeInformativoErrorModal").modal("show");
      this.retigistroRetencionJudicial.tipoProceso = this.listaTipoProceso;
      this.imagenCargando = false;
    });
  }

  modificarTipoProcesoEnviar() {
    if (this.retigistroRetencionJudicial.tipoProceso.length > 0) {
      this.listaTipoProceso = this.retigistroRetencionJudicial.tipoProceso;
      var valor: any = this.retigistroRetencionJudicial.tipoProceso;
      var cadenafinal: string = '';
      for (let index = 0; index < valor.length; index++) {
        if (index == 0) {
          cadenafinal = cadenafinal.concat(valor[index]);
        } else {
          cadenafinal = cadenafinal.concat(',').concat(valor[index]);
        }
      }
      this.retigistroRetencionJudicial.tipoProceso = cadenafinal;
    }
  }

  validarDatos() {
    this.imagenCargando = true;
    if (this.validarEnvioConsultaJuzgado() && this.validarPorcentaje() &&
      this.validarCusppDemandado() && this.validarMontoTope() &&
      this.validarMontoDescuento() && this.validarAbonoCuentaNumeroCuenta() &&
      this.validarCiaSeguros()) {
      $("#confirmacionNuevaRetencionModal").modal({ backdrop: "static" });
      this.imagenCargando = false;
    } else {
      $("#validacionDatosNuevaRetencionModal").modal("show");
      this.imagenCargando = false;
    }
  }

  validarEnvioConsultaJuzgado() {
    if (this.retigistroRetencionJudicial.seEnviaConsuldoJuzgado !== '' && this.retigistroRetencionJudicial.seEnviaConsuldoJuzgado != null && this.retigistroRetencionJudicial.seEnviaConsuldoJuzgado != undefined) {
      return true
    } else {
      this.mensajeError = 'Seleccione si se envia al juzgado';
      return false
    }
  }

  validarMontoTope() {
    if (this.retigistroRetencionJudicial.montoTope <= 999999999999999) {
      return true
    } else {
      this.mensajeError = 'Sobrepasa el maximo permitido del monto tope';
      return false
    }
  }

  validarAbonoCuentaNumeroCuenta() {
    switch (this.retigistroRetencionJudicial.formaPago) {
      case 'A':
        if (this.retigistroRetencionJudicial.numCuentaDepo != '') {
          return true
        } else {
          this.mensajeError = 'Debe de ingresar Numero de Cuenta';
          return false
        }
      default:
        return true;
    }
  }

  validarCiaSeguros() {
    switch (this.retigistroRetencionJudicial.encargadoRetencion) {
      case 'CIA':
        if (this.retigistroRetencionJudicial.ciaRetencion != '') {
          return true
        } else {
          this.mensajeError = 'Debe de seleccionar una Cia de seguro a cargo de retención';
          return false
        }
      default:
        return true;
    }
  }

  validarMontoDescuento() {
    if (this.retigistroRetencionJudicial.montoDescuento <= 999999999999999) {
      return true
    } else {
      this.mensajeError = 'Sobrepasa el maximo permitido del monto de descuento';
      return false
    }
  }

  validarCusppDemandado() {
    if (this.retigistroRetencionJudicial.cusppDemandado !== '' && this.retigistroRetencionJudicial.cusppDemandado != undefined && this.retigistroRetencionJudicial.cusppDemandado != null) {
      return true;
    } else {
      this.mensajeError = 'Debe Seleccionar una persona con numero de CUSPP del demandado';
      return false;
    }
  }

  validarPorcentaje() {
    if (this.retigistroRetencionJudicial.porcentajeDescuento <= 100) {
      return true;
    } else if (this.retigistroRetencionJudicial.porcentajeDescuento == 0) {
      return true;
    } else {
      this.mensajeError = 'El porcentaje no puede ser mayor al 100%';
      return false;
    }
  }

  regresarPaginaInicio() {
    $("#validacionconfirmacionNuevaRetencionModal").modal("hide");
    this.router.navigate(['listar-retenciones']);
  }

  cargarPasos() {
    this.pasos = [{
      "id": 1,
      "label": "Paso 1",
      "descripcion": "Agregar demandado",
      "link": "#informacion",
      "active": "active",
      "open": true
    },
    {
      "id": 2,
      "label": "Paso 2",
      "descripcion": "Validar datos demandado",
      "link": "#suscripcion",
      "active": "",
      "open": false
    },

    {

      "id": 3,
      "label": "Paso 3",
      "descripcion": "Agregar datos demandante",
      "link": "#factura",
      "active": "",
      "open": false
    }, {

      "id": 4,
      "label": "Paso 4",
      "descripcion": "Agregar indicadores",
      "link": "#pago",
      "active": "",
      "open": false
    }]
  }


}
