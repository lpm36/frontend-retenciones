import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { RetencionAfiliado } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-afiliado';
import { RetencionBeneficiario } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-beneficiario';
import { forkJoin } from 'rxjs';
import { ListarAfiliadoBody } from 'projects/retencion-judicial/src/app/util/interfaces/listar-afiliado-body';
import { ListarBeneficiarioBody } from 'projects/retencion-judicial/src/app/util/interfaces/listar-beneficiario-body';
declare var $: any;

@Component({
  selector: 'app-inicio-buscar-persona',
  templateUrl: './inicio-buscar-persona.component.html',
  styleUrls: ['./inicio-buscar-persona.component.css']
})
export class InicioBuscarPersonaComponent implements OnInit {

  tipoBusqueda: any[];
  selectTipBus: any;
  imagenCargando: boolean = false;
  arryAfiliado: RetencionAfiliado[];
  arryBeneficiario: RetencionBeneficiario[];
  pesonaAfiliado: RetencionAfiliado = new RetencionAfiliado();
  pesonaBeneficiario: RetencionBeneficiario = new RetencionBeneficiario();
  afili: number = 0;
  benefi: number = 0;
  cabeceraTabla: string[] = [];
  total: any;
  priNombre: string = '';
  segNombre: string = '';
  apePaterno: string = '';
  apeMaterno: string = '';
  numDoc: string = '';
  afiliadoBody: ListarAfiliadoBody = new ListarAfiliadoBody();
  beneficiarioBody: ListarBeneficiarioBody = new ListarBeneficiarioBody();
  @Output() personaSeleccionadaAfiliado = new EventEmitter<RetencionAfiliado>();
  @Output() personaSeleccionadaBeneficiario = new EventEmitter<RetencionBeneficiario>();

  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService
  ) {
    this.inicializar();
  }

  ngOnInit() {
    this.selectTipBus = this.tipoBusqueda[0];
  }
  inicializar() {
    this.tipoBusqueda = [
      { id: 1, name: 'Nombre de Persona' },
      { id: 2, name: 'DNI' }
    ];
    this.cargartablaCabecera();
    this.inicializarArreglos();
  }
  buscar() {
    if (
      this.apeMaterno != '' || this.apePaterno != '' || this.priNombre != '' ||
      this.segNombre != '' || this.numDoc != ''
    ) {
      this.imagenCargando = true;
      this.cargarBodyAfiliado(0, 0);
      this.cargarBodyBeneficiario(0, 0);
      const getAfiliado = this._servicioRJ.postListarAfiliado(this.afiliadoBody);
      const getBeneficiario = this._servicioRJ.postListarBeneficiario(this.beneficiarioBody);
      forkJoin([getBeneficiario, getAfiliado]).subscribe(resultado => {
        if (resultado[0].length > 0) {
          this.benefi = 1;
          this.afili = 0;
          this.total = resultado[0].length;
          this.cargarListaBeneficiario();
        } else if (resultado[1].length > 0) {
          this.afili = 1;
          this.benefi = 0;
          this.total = resultado[1].length;
          this.cargarListaAfiliado();
        } else {
          this.imagenCargando = false;
        }
      }, () => {
        this.imagenCargando = false;
      });
    }
  }

  cargarBodyAfiliado(pagina: number = 1, cantPagina: number = 10, total: boolean = true) {
    this.afiliadoBody.apeMaterno = this.apeMaterno.trim().toUpperCase();
    this.afiliadoBody.apePaterno = this.apePaterno.trim().toUpperCase();
    this.afiliadoBody.priNombre = this.priNombre.trim().toUpperCase();
    this.afiliadoBody.segNombre = this.segNombre.trim().toUpperCase();
    this.afiliadoBody.numDoc = this.numDoc.trim().toUpperCase();
    this.afiliadoBody.cuspp = '';
    this.afiliadoBody.cantPorPagina = cantPagina;
    this.afiliadoBody.pagina = pagina;
    this.afiliadoBody.total = total;
  }

  cargarBodyBeneficiario(pagina: number = 1, cantPagina: number = 10, total: boolean = true) {
    this.beneficiarioBody.beneApeMaterno = this.apeMaterno.trim().toUpperCase();
    this.beneficiarioBody.beneApePaterno = this.apePaterno.trim().toUpperCase();
    this.beneficiarioBody.benePriNombre = this.priNombre.trim().toUpperCase();
    this.beneficiarioBody.beneSegNombre = this.segNombre.trim().toUpperCase();
    this.beneficiarioBody.beneNumDoc = this.numDoc.trim().toUpperCase();
    this.beneficiarioBody.beneCuspp = '';
    this.beneficiarioBody.beneCantPorPagina = cantPagina;
    this.beneficiarioBody.benPagina = pagina;
    this.beneficiarioBody.beneTotal = total;
  }

  cargartablaCabecera() {
    this.cabeceraTabla = [];
    this.cabeceraTabla.push('Elegir');
    this.cabeceraTabla.push('Nro. Cuspp');
    this.cabeceraTabla.push('Nro. DNI');
    this.cabeceraTabla.push('Nombre de la persona');
  }

  cargarCabeceraBeneficiario() {
    this.cabeceraTabla = [];
    this.cabeceraTabla.push('Elegir');
    this.cabeceraTabla.push('Nro. Cuspp');
    this.cabeceraTabla.push('Nro. DNI');
    this.cabeceraTabla.push('Nro. Benf.');
    this.cabeceraTabla.push('Tipo');
    this.cabeceraTabla.push('Nombre de la persona');
  }

  inicializarArreglos() {
    this.arryAfiliado = [];
    this.arryBeneficiario = [];
  }

  paginacion(objeto: any) {
    if (this.afili == 1) {
      this.pesonaAfiliado = objeto;
    } else if (this.benefi == 1) {
      this.pesonaBeneficiario = objeto;
    }
  }

  enviarPersona() {
    this.personaSeleccionadaAfiliado.emit(this.pesonaAfiliado);
    this.personaSeleccionadaBeneficiario.emit(this.pesonaBeneficiario);
    this.limpiar();
  }

  cancelar() {
    this.limpiar();
    $("#inicioBuscarPersonaModal").modal("hide");
  }

  limpiar() {
    this.inicializarArreglos();
    this.afili = 0;
    this.benefi = 0;
    this.total = 0;
    this.total = 0;
    this.priNombre = '';
    this.segNombre = '';
    this.apePaterno = '';
    this.apeMaterno = '';
    this.numDoc = '';
  };

  paginate(evento: any) {
    const page = evento.page + 1;
    if (this.afili == 1) {
      this.cargarListaAfiliado(page);
    } else if (this.benefi == 1) {
      this.cargarListaBeneficiario(page);
    };
  }

  cargarListaAfiliado(pagina: number = 1, cantPagina: number = 10) {
    this.imagenCargando = true;
    this.cargarBodyAfiliado(pagina, cantPagina);
    this._servicioRJ.postListarAfiliado(this.afiliadoBody).subscribe(data => {
      this.cargartablaCabecera();
      this.arryAfiliado = data;
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarListaBeneficiario(pagina: number = 1, cantPagina: number = 10) {
    this.imagenCargando = true;
    this.cargarBodyBeneficiario(pagina, cantPagina);
    this._servicioRJ.postListarBeneficiario(this.beneficiarioBody).subscribe(data => {
      this.cargarCabeceraBeneficiario();
      this.arryBeneficiario = data;
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }
}
