import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaRetencionComponent } from './nueva-retencion.component';

describe('NuevaRetencionComponent', () => {
  let component: NuevaRetencionComponent;
  let fixture: ComponentFixture<NuevaRetencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaRetencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaRetencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
