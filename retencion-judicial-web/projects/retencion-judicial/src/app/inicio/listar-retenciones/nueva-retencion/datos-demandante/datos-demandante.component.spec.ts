import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosDemandanteComponent } from './datos-demandante.component';

describe('DatosDemandanteComponent', () => {
  let component: DatosDemandanteComponent;
  let fixture: ComponentFixture<DatosDemandanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosDemandanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosDemandanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
