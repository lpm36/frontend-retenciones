import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RetigistroRetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retigistro-retencion-judicial';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { forkJoin, Observable } from 'rxjs';
import { retencionPensionistaAfp } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-pensionista-afp';

@Component({
  selector: 'app-datos-demandado',
  templateUrl: './datos-demandado.component.html',
  styleUrls: ['./datos-demandado.component.css']
})
export class DatosDemandadoComponent implements OnInit {

  @Input('retencionDemandado') retRetJud: RetigistroRetencionJudicial;
  @Output() retornarRetJudDatosDemandado = new EventEmitter<RetigistroRetencionJudicial>();
  imagenCargando: boolean = false;
  condicionDemandante: string;
  montoUltimaPension: string;
  afectado: string = '';
  cuspp: string;
  secuencia: number;
  apePaterno: string;
  apeMaterno: string;
  priNombre: string;
  segNombre: string;
  sexo: string;
  tipoDocumento: string;
  numDocAfiliado: string;
  numBeneficiario: string;
  secBeneficiario: string;
  constructor(
    public _servicioRJ: ServiciosRetencionesJudicialesService
  ) {

  }

  ngOnInit() {
    this.cargarDatosFormulario();
    this.cargarDatosRestantes();
  }

  cargarDatosFormulario() {
    this.condicionDemandante = '';
    this.montoUltimaPension = '';
    this.afectado = (this.retRetJud.afectaDemandado == 'A' ? 'AFILIADO' : 'BENEFICIARIO');
    this.cuspp = (this.retRetJud.cusppDemandado);
    this.secuencia = (0);
    this.apePaterno = (this.retRetJud.apePatDemandado);
    this.apeMaterno = (this.retRetJud.apeMatDemandado);
    this.priNombre = (this.retRetJud.priNomDemandado);
    this.segNombre = (this.retRetJud.segNomDemandado);
    this.sexo = (this.retRetJud.sexoDemandado.trim() === 'M' ? 'Masculino' : 'Femenino');
    this.tipoDocumento = (this.retRetJud.tipoDocDemandado);
    this.numDocAfiliado = (this.retRetJud.numDocDemandado);
    this.numBeneficiario = (this.retRetJud.descripNumBeneficiarioDemandando);
    this.secBeneficiario = (this.retRetJud.secBeneficiarioDemandado);
  }

  cargarDatosRestantes() {
    this.imagenCargando = true;
    let getPensionista: Observable<retencionPensionistaAfp[]>;
    if (this.retRetJud.afectaDemandado == 'A') {
      getPensionista = this._servicioRJ.getListarPensinista(this.retRetJud.cusppDemandado);
    } else if (this.retRetJud.afectaDemandado == 'B') {
      const tipo = sessionStorage.getItem('tipoBeneficiario');
      const nume = parseInt(sessionStorage.getItem('numBeneficiario'));
      getPensionista = this._servicioRJ.getListarPensinista(this.retRetJud.cusppDemandado, tipo, nume);
    }
    //const getRetenciones = this._servicioRJ.getListarRetenciones(0, '', this.retRetJud.numDocDemandado, true);
    const getRetenciones = this._servicioRJ.getListarRetenciones('', '', '', '', this.retRetJud.numDocDemandado, 0, true);
    const getValidarPensionista = this._servicioRJ.getValidarPensionista(this.retRetJud.cusppDemandado);
    forkJoin([getPensionista, getRetenciones, getValidarPensionista]).subscribe(resultado => {
      if (resultado[2] > 0) {
        this.condicionDemandante = 'Pensionista';
        this.montoUltimaPension = (resultado[0].length > 0 ? this.montoPension(resultado[0]) : '');
      } else {
        this.condicionDemandante = 'No Pensionista';
        this.montoUltimaPension = '0';
      }
      if (resultado[1].length > 0) {
        this.generarSecuencia(resultado[1]);
      } else if (resultado[1].length <= 0) {
        this.secuencia = (1);
      }
      this.emitirDatosRestantes();
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }

  generarSecuencia(dato: any[]) {
    const numObj = dato.length - 1;
    const secuenciaIncrementada = (dato[numObj].secuencia + 1);
    this.secuencia = (secuenciaIncrementada);
  }

  montoPension(dato: any[]) {
    const numObj = dato.length - 1;
    return dato[numObj].montoActualPension;
  }

  emitirDatosRestantes() {
    this.retRetJud.secuenciaDemandado = this.secuencia;
    this.retornarRetJudDatosDemandado.emit(this.retRetJud);
  }


}
