import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioNuevaRetencionComponent } from './inicio-nueva-retencion.component';

describe('InicioNuevaRetencionComponent', () => {
  let component: InicioNuevaRetencionComponent;
  let fixture: ComponentFixture<InicioNuevaRetencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioNuevaRetencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioNuevaRetencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
