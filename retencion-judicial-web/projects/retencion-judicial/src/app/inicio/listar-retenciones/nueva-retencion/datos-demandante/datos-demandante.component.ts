import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-judicial';
import { ServiciosRetencionesJudicialesService } from 'projects/retencion-judicial/src/app/util/services/servicios-retenciones-judiciales.service';
import { forkJoin } from 'rxjs';
import { TipoIdentificacion } from 'projects/retencion-judicial/src/app/util/interfaces/tipo-identificacion';
import { RetencionDepartamento } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-departamento';
import { RetigistroRetencionJudicial } from 'projects/retencion-judicial/src/app/util/interfaces/retigistro-retencion-judicial';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListaValor } from 'projects/retencion-judicial/src/app/util/interfaces/lista-valor';
import { RetencionCiudadProvincia } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-ciudad-provincia';
import { RetencionDistrito } from 'projects/retencion-judicial/src/app/util/interfaces/retencion-distrito';

@Component({
  selector: 'app-datos-demandante',
  templateUrl: './datos-demandante.component.html',
  styleUrls: ['./datos-demandante.component.css']
})
export class DatosDemandanteComponent implements OnInit {

  @Input('retigistroDemandante') retRetJud: RetigistroRetencionJudicial;
  @Output() retornarRetJudDatosDemanante = new EventEmitter<RetigistroRetencionJudicial>();
  tipoDocumentoCobrante: string;
  tipoDocumentoAlimentista: string;

  generarReintegro: any[];
  arrayTipoDocumento: TipoIdentificacion[];
  arrayDepartamento: RetencionDepartamento[];
  arraySexo: ListaValor[];
  arrayPais: ListaValor[] = [];
  arrayProvinciaCiudad: RetencionCiudadProvincia[];
  arrayDistrito: RetencionDistrito[];
  contador: number = 0;
  selectedGenerarReintegro: string;
  maxDate: Date;
  minDate: Date;
  yearRange: string;
  es: any;
  fechaActual: string;
  retencionjudicial: RetencionJudicial = new RetencionJudicial();
  selectedValue: string;
  selectedValue2: string;
  selectedValue3: string;
  imagenCargando: boolean = false;
  formGeneral: FormGroup;
  formDatosDemandante: FormGroup;
  formDatosCobrante: FormGroup;
  formDatosAlimentista: FormGroup;
  formDatosCombosJuzgado: FormGroup;
  formDatosJuzgado: FormGroup;

  constructor(
    private _servicioRJ: ServiciosRetencionesJudicialesService,
    private fbGeneral: FormBuilder,
    private fbDemandante: FormBuilder,
    private fbCobrante: FormBuilder,
    private fbAlimentista: FormBuilder,
    private fbCombosJuzgado: FormBuilder,
    private fbJuzgado: FormBuilder
  ) {

  }

  ngOnInit() {
    this.cargarFormularios();
    this.cargarInicial();
  }

  cargarInicial() {
    this.cargarDatos();
    this.fechaMaxMin();
    this.cargarFecha();
    this.paices();
    this.cargarMesesEsp();
    this.cargarCombos();
    this.precargado();
  }

  cargarFormularios() {
    this.cargarGeneralFormulario();
    this.cargarDemandanteFormulario();
    this.cargarCobranteFormulario();
    this.cargarAlimentistaFormulario();
    this.cargarJuzgadoCombosFormulario();
    this.cargarJuzgadoFormulario();
  }

  paices() {
    let pais: ListaValor = new ListaValor();
    pais.columValor = '';
    pais.valor = 'PE';
    pais.descripcion = 'PERÚ';
    this.arrayPais.push(pais);
  }

  cargarDatos() {
    this.generarReintegro = [
      { label: 'Si', value: 'S' },
      { label: 'No', value: 'N' }
    ];
  }

  precargado() {
    this.formDatosCombosJuzgado.controls['paisJuzgado'].setValue('PE');
  }

  fechaMaxMin() {
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let nextMonth = (month === 11) ? 0 : month;
    let nextYear = (nextMonth === 0) ? year : year;
    let prevMonth = (month === 0) ? 11 : month;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    this.fechaActual = new Date().toLocaleDateString();//this.validarMes(today.getDate(), month, year);
    this.maxDate = new Date();
    this.maxDate.setMonth(nextMonth);
    this.maxDate.setFullYear(nextYear);
    this.minDate = new Date();
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);
  }

  validarMes(dia: number, mes: number, anio: number) {
    const diaf = (dia < 10 ? '0' + dia.toString() : mes.toString());
    let mesF = (mes < 10 ? '0' + mes.toString() : mes.toString());
    return diaf + '/' + mesF + '/' + anio.toString();
  }

  cargarFecha() {
    this.yearRange = "1994" + ":" + (new Date().getFullYear().toString());
  }

  cargarCombos() {
    this.imagenCargando = true;
    const getTipoDocumento = this._servicioRJ.getTipoIdentificacion(0);
    const getDepartamento = this._servicioRJ.getListarDepartamento();
    const getSexo = this._servicioRJ.getTipoSexo();
    forkJoin([getTipoDocumento, getDepartamento, getSexo]).subscribe(resultado => {
      this.arrayTipoDocumento = resultado[0];
      this.arrayDepartamento = resultado[1];
      this.arraySexo = resultado[2];
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarDemandanteFormulario() {
    this.formDatosDemandante = this.fbDemandante.group({
      apePaternoDemantante: [this.retRetJud.apePatDemandante != undefined ? this.retRetJud.apePatDemandante : ''],
      apeMaternoDemandante: [this.retRetJud.apeMatDemandante != undefined ? this.retRetJud.apeMatDemandante : ''],
      primerNombreDemandante: [this.retRetJud.priNomDemandante != undefined ? this.retRetJud.priNomDemandante : ''],
      segundoNombreDemandante: [this.retRetJud.segNomDemandante != undefined ? this.retRetJud.segNomDemandante : ''],
      sexoDemandante: [this.retRetJud.sexoDemandante != undefined ? this.retRetJud.sexoDemandante : ''],
      tipoDocDemandante: [this.retRetJud.tipoDocDemandante != undefined ? this.retRetJud.tipoDocDemandante : ''],
      numDocDemandante: [this.retRetJud.numDocDemandante != undefined ? this.retRetJud.numDocDemandante : ''],
      cusppDemandante: [this.retRetJud.cusppDemandante != undefined ? this.retRetJud.cusppDemandante : ''],
      sacDemandante: [this.retRetJud.sacDemandante != undefined ? this.retRetJud.sacDemandante : ''],
      apePaternoCobrante: [this.retRetJud.apePatCobrante != undefined ? this.retRetJud.apePatCobrante : '']
    });
    this.formDatosDemandante.valueChanges.subscribe(data => {
      this.retRetJud.apePatDemandante = data.apePaternoDemantante;
      this.retRetJud.apeMatDemandante = data.apeMaternoDemandante;
      this.retRetJud.priNomDemandante = data.primerNombreDemandante;
      this.retRetJud.segNomDemandante = data.segundoNombreDemandante;
      this.retRetJud.sexoDemandante = data.sexoDemandante;
      this.retRetJud.tipoDocDemandante = data.tipoDocDemandante;
      this.retRetJud.numDocDemandante = data.numDocDemandante;
      this.retRetJud.cusppDemandante = data.cusppDemandante;
      this.retRetJud.sacDemandante = data.sacDemandante;
      this.retornarRetJudDatosDemanante.emit(this.retRetJud);
    });
  }

  cargarGeneralFormulario() {
    this.formGeneral = this.fbGeneral.group({
      selectedGenerarReintegro: [(this.retRetJud.genrarReintegroSiNo != null && this.retRetJud.genrarReintegroSiNo != undefined ? this.cargarObjetoReintegro() : '')],
      ngFechaDesde: [(this.retRetJud.fchDesde != null ? this.devolverformatoFecha(this.retRetJud.fchDesde) : '')],
    });
    this.formGeneral.valueChanges.subscribe(data => {
      this.retRetJud.genrarReintegroSiNo = data.selectedGenerarReintegro.value;
      this.retRetJud.fchDesde = this.transformarFechaAs(data.ngFechaDesde != '' ? new Date(data.ngFechaDesde).toLocaleDateString() : '');
      this.retornarRetJudDatosDemanante.emit(this.retRetJud);
    });
  }
  cargarObjetoReintegro() {
    if (this.retRetJud.genrarReintegroSiNo === 'S') {
      return { label: 'Si', value: 'S' };
    } else if (this.retRetJud.genrarReintegroSiNo === 'N') {
      return { label: 'No', value: 'N' };
    }
  }

  cargarCobranteFormulario() {
    this.formDatosCobrante = this.fbCobrante.group({
      apePaternoCobrante: [this.retRetJud.apePatCobrante != undefined ? this.retRetJud.apePatCobrante : ''],
      apeMaternoCobrante: [this.retRetJud.apeMatCobrante != undefined ? this.retRetJud.apeMatCobrante : ''],
      primerNombreCobrante: [this.retRetJud.priNomCobrante != undefined ? this.retRetJud.priNomCobrante : ''],
      segundoNombreCobrante: [this.retRetJud.segNomCobrante != undefined ? this.retRetJud.segNomCobrante : ''],
      sexoCobrante: [this.retRetJud.sexoCobrante != undefined ? this.retRetJud.sexoCobrante : ''],
      tipoDocCobrante: [this.retRetJud.tipoDocCobrante != undefined ? this.retRetJud.tipoDocCobrante : ''],
      numDocCobrante: [this.retRetJud.numDocCobrante != undefined ? this.retRetJud.numDocCobrante : ''],
      cusppCobrante: [this.retRetJud.cusppCobrante != undefined ? this.retRetJud.cusppCobrante : ''],
      sacCobrante: [this.retRetJud.sacCobrante != undefined ? this.retRetJud.sacCobrante : '']
    });
    this.formDatosCobrante.valueChanges.subscribe(data => {
      this.retRetJud.apePatCobrante = data.apePaternoCobrante;
      this.retRetJud.apeMatCobrante = data.apeMaternoCobrante;
      this.retRetJud.priNomCobrante = data.primerNombreCobrante;
      this.retRetJud.segNomCobrante = data.segundoNombreCobrante;
      this.retRetJud.sexoCobrante = data.sexoCobrante;
      this.retRetJud.tipoDocCobrante = data.tipoDocCobrante;
      this.retRetJud.numDocCobrante = data.numDocCobrante;
      this.retRetJud.cusppCobrante = data.cusppCobrante;
      this.retRetJud.sacCobrante = data.sacCobrante;
      this.retornarRetJudDatosDemanante.emit(this.retRetJud);
    });
  }

  cargarAlimentistaFormulario() {
    this.formDatosAlimentista = this.fbAlimentista.group({
      apePaternoAlimentista: [this.retRetJud.apePatAlimentista != undefined ? this.retRetJud.apePatAlimentista : ''],
      apeMaternoAlimentista: [this.retRetJud.apeMatAlimentista != undefined ? this.retRetJud.apeMatAlimentista : ''],
      primerNombreAlimentista: [this.retRetJud.priNomAlimentista != undefined ? this.retRetJud.priNomAlimentista : ''],
      segundoNombreAlimentista: [this.retRetJud.segNomAlimentista != undefined ? this.retRetJud.segNomAlimentista : ''],
      sexoAlimentista: [this.retRetJud.sexoAlimentista != undefined ? this.retRetJud.sexoAlimentista : ''],
      tipoDocAlimentista: [this.retRetJud.tipoDocAlimentista != undefined ? this.retRetJud.tipoDocAlimentista : ''],
      numDocAlimentista: [this.retRetJud.numDocAlimentista != undefined ? this.retRetJud.numDocAlimentista : ''],
      cusppAlimentista: [this.retRetJud.cusppAlimentista != undefined ? this.retRetJud.cusppAlimentista : ''],
      sacAlimentista: [this.retRetJud.sacAlimentista != undefined ? this.retRetJud.sacAlimentista : '']
    });
    this.formDatosAlimentista.valueChanges.subscribe(data => {
      this.retRetJud.apePatAlimentista = data.apePaternoAlimentista;
      this.retRetJud.apeMatAlimentista = data.apeMaternoAlimentista;
      this.retRetJud.priNomAlimentista = data.primerNombreAlimentista;
      this.retRetJud.segNomAlimentista = data.segundoNombreAlimentista;
      this.retRetJud.sexoAlimentista = data.sexoAlimentista;
      this.retRetJud.tipoDocAlimentista = data.tipoDocAlimentista;
      this.retRetJud.numDocAlimentista = data.numDocAlimentista;
      this.retRetJud.cusppAlimentista = data.cusppAlimentista;
      this.retRetJud.sacAlimentista = data.sacAlimentista;
      this.retornarRetJudDatosDemanante.emit(this.retRetJud);
    });
  }

  cargarJuzgadoCombosFormulario() {
    this.formDatosCombosJuzgado = this.fbCombosJuzgado.group({
      paisJuzgado: [this.retRetJud.pais != undefined ? this.retRetJud.pais : ''],
      departamentoJuzgado: [this.retRetJud.departamento != undefined ? this.retRetJud.departamento : ''],
      provinciaJuzgado: [this.retRetJud.provincia != undefined ? this.retRetJud.provincia : ''],
      distritoJuzgado: [this.retRetJud.distrito != undefined ? this.retRetJud.distrito : ''],
    });
    this.formDatosCombosJuzgado.valueChanges.subscribe(data => {
      this.retRetJud.pais = data.paisJuzgado;
      this.retRetJud.departamento = data.departamentoJuzgado;
      this.retRetJud.provincia = data.provinciaJuzgado;
      this.retRetJud.distrito = data.distritoJuzgado;
      this.cargarProvinciaDistrito();
      this.retornarRetJudDatosDemanante.emit(this.retRetJud);
    });
  }

  cargarJuzgadoFormulario() {
    this.formDatosJuzgado = this.fbJuzgado.group({
      direccionJuzgado: [this.retRetJud.direccionJuzgado !== undefined ? this.retRetJud.direccionJuzgado : ''],
      nombreJuzgado: [this.retRetJud.nombreJuzgado != undefined ? this.retRetJud.nombreJuzgado : ''],
      numOficio: [this.retRetJud.numOficio != undefined ? this.retRetJud.numOficio : ''],
      numExpediente: [this.retRetJud.numExpediente != undefined ? this.retRetJud.numExpediente : '']
    });
    this.formDatosJuzgado.valueChanges.subscribe(data => {
      this.retRetJud.direccionJuzgado = data.direccionJuzgado;
      this.retRetJud.nombreJuzgado = data.nombreJuzgado;
      this.retRetJud.numOficio = data.numOficio;
      this.retRetJud.numExpediente = data.numExpediente;
      this.retornarRetJudDatosDemanante.emit(this.retRetJud);
    });
  }

  cargarProvinciaDistrito() {
    if (this.retRetJud.departamento.length > 0) {
      this.cargarProvincia();
    }
    if (this.retRetJud.provincia.length > 0) {
      this.cargarDistrito();
    }
  }

  cargarProvincia() {
    this.imagenCargando = true;
    this._servicioRJ.getCiudadProvincia(this.retRetJud.departamento).subscribe(data => {
      this.arrayProvinciaCiudad = data;
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }

  cargarDistrito() {
    this.imagenCargando = true;
    this._servicioRJ.getDistrito('', this.retRetJud.provincia, this.retRetJud.departamento).subscribe(data => {
      this.arrayDistrito = data;
      this.imagenCargando = false;
    }, () => {
      this.imagenCargando = false;
    });
  }

  copiarDatosDemandante() {
    this.formDatosCobrante.controls['apePaternoCobrante'].setValue(this.retRetJud.apePatDemandante);
    this.formDatosCobrante.controls['apeMaternoCobrante'].setValue(this.retRetJud.apeMatDemandante);
    this.formDatosCobrante.controls['primerNombreCobrante'].setValue(this.retRetJud.priNomDemandante);
    this.formDatosCobrante.controls['segundoNombreCobrante'].setValue(this.retRetJud.segNomDemandante);
    this.formDatosCobrante.controls['sexoCobrante'].setValue(this.retRetJud.sexoDemandante);
    this.formDatosCobrante.controls['tipoDocCobrante'].setValue(this.retRetJud.tipoDocDemandante);
    this.formDatosCobrante.controls['numDocCobrante'].setValue(this.retRetJud.numDocDemandante);
    this.formDatosCobrante.controls['cusppCobrante'].setValue(this.retRetJud.cusppDemandante);
    this.formDatosCobrante.controls['sacCobrante'].setValue(this.retRetJud.sacDemandante);

    this.retRetJud.apePatCobrante = this.retRetJud.apePatDemandante;
    this.retRetJud.apeMatCobrante = this.retRetJud.apeMatDemandante;
    this.retRetJud.priNomCobrante = this.retRetJud.priNomDemandante;
    this.retRetJud.segNomCobrante = this.retRetJud.segNomDemandante;
    this.retRetJud.sexoCobrante = this.retRetJud.sexoDemandante;
    this.retRetJud.tipoDocCobrante = this.retRetJud.tipoDocDemandante;
    this.retRetJud.numDocCobrante = this.retRetJud.numDocDemandante;
    this.retRetJud.cusppCobrante = this.retRetJud.cusppDemandante;
    this.retRetJud.sacCobrante = this.retRetJud.sacDemandante;
    this.retornarRetJudDatosDemanante.emit(this.retRetJud);
  }
  copiarDatosDemandanteALimentista() {
    this.formDatosAlimentista.controls['apePaternoAlimentista'].setValue(this.retRetJud.apePatDemandante);
    this.formDatosAlimentista.controls['apeMaternoAlimentista'].setValue(this.retRetJud.apeMatDemandante);
    this.formDatosAlimentista.controls['primerNombreAlimentista'].setValue(this.retRetJud.priNomDemandante);
    this.formDatosAlimentista.controls['segundoNombreAlimentista'].setValue(this.retRetJud.segNomDemandante);
    this.formDatosAlimentista.controls['sexoAlimentista'].setValue(this.retRetJud.sexoDemandante);
    this.formDatosAlimentista.controls['tipoDocAlimentista'].setValue(this.retRetJud.tipoDocDemandante);
    this.formDatosAlimentista.controls['numDocAlimentista'].setValue(this.retRetJud.numDocDemandante);
    this.formDatosAlimentista.controls['cusppAlimentista'].setValue(this.retRetJud.cusppDemandante);
    this.formDatosAlimentista.controls['sacAlimentista'].setValue(this.retRetJud.sacDemandante);

    this.retRetJud.apePatAlimentista = this.retRetJud.apePatDemandante;
    this.retRetJud.apeMatAlimentista = this.retRetJud.apeMatDemandante;
    this.retRetJud.priNomAlimentista = this.retRetJud.priNomDemandante;
    this.retRetJud.segNomAlimentista = this.retRetJud.segNomDemandante;
    this.retRetJud.sexoAlimentista = this.retRetJud.sexoDemandante;
    this.retRetJud.tipoDocAlimentista = this.retRetJud.tipoDocDemandante;
    this.retRetJud.numDocAlimentista = this.retRetJud.numDocDemandante;
    this.retRetJud.cusppAlimentista = this.retRetJud.cusppDemandante;
    this.retRetJud.sacAlimentista = this.retRetJud.sacDemandante;
    this.retornarRetJudDatosDemanante.emit(this.retRetJud);
  }

  copiarDatosCobranteALimentista() {
    this.formDatosAlimentista.controls['apePaternoAlimentista'].setValue(this.retRetJud.apePatCobrante);
    this.formDatosAlimentista.controls['apeMaternoAlimentista'].setValue(this.retRetJud.apeMatCobrante);
    this.formDatosAlimentista.controls['primerNombreAlimentista'].setValue(this.retRetJud.priNomCobrante);
    this.formDatosAlimentista.controls['segundoNombreAlimentista'].setValue(this.retRetJud.segNomCobrante);
    this.formDatosAlimentista.controls['sexoAlimentista'].setValue(this.retRetJud.sexoCobrante);
    this.formDatosAlimentista.controls['tipoDocAlimentista'].setValue(this.retRetJud.tipoDocCobrante);
    this.formDatosAlimentista.controls['numDocAlimentista'].setValue(this.retRetJud.numDocCobrante);
    this.formDatosAlimentista.controls['cusppAlimentista'].setValue(this.retRetJud.cusppCobrante);
    this.formDatosAlimentista.controls['sacAlimentista'].setValue(this.retRetJud.sacCobrante);

    this.retRetJud.apePatAlimentista = this.retRetJud.apePatCobrante;
    this.retRetJud.apeMatAlimentista = this.retRetJud.apeMatCobrante;
    this.retRetJud.priNomAlimentista = this.retRetJud.priNomCobrante;
    this.retRetJud.segNomAlimentista = this.retRetJud.segNomCobrante;
    this.retRetJud.sexoAlimentista = this.retRetJud.sexoCobrante;
    this.retRetJud.tipoDocAlimentista = this.retRetJud.tipoDocCobrante;
    this.retRetJud.numDocAlimentista = this.retRetJud.numDocCobrante;
    this.retRetJud.cusppAlimentista = this.retRetJud.cusppCobrante;
    this.retRetJud.sacAlimentista = this.retRetJud.sacCobrante;
    this.retornarRetJudDatosDemanante.emit(this.retRetJud);
  }

  transformarFechaAs(fecha: string) {
    if (fecha.length > 0) {
      let split = fecha.split('/');
      return split[2] + (parseInt(split[1]) < 10 ? '0' + split[1] : split[1]) + (parseInt(split[0]) < 10 ? '0' + split[0] : split[0]);
    } else {
      return '';
    }
  }

  cargarMesesEsp() {
    this.es = {
      firstDayOfWeek: 1,
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      today: 'Hoy',
      clear: 'Borrar'
    }
  }

  devolverformatoFecha(fecha: string) {
    if (fecha.length > 0) {
      let anio: string = fecha.charAt(0) + fecha.charAt(1) + fecha.charAt(2) + fecha.charAt(3);
      let mes: string = fecha.charAt(4) + fecha.charAt(5);
      let dia: string = fecha.charAt(6) + fecha.charAt(7);
      return dia.concat('/').concat(mes).concat('/').concat(anio);
    } else {
      return '';
    }
  }

}
