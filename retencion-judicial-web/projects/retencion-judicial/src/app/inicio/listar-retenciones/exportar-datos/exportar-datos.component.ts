import { Component, OnInit, EventEmitter, Output, Input} from '@angular/core';

@Component({
  selector: 'app-exportar-datos',
  templateUrl: './exportar-datos.component.html',
  styleUrls: ['./exportar-datos.component.css']
})
export class ExportarDatosComponent implements OnInit {

  @Input() ngFechaInicial: Date;
  @Input() ngFechaFinal: Date;
  yearRange: string;
  es: any;
  maxDate: Date;
  minDate: Date;
  @Output() valoresFiltroDescarga = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {  
    this.cargarDatosIniciales();
  }

  cargarDatosIniciales(){
    this.cargarFecha();
    this.cargarMesesEsp();
    this.fechaMaxMin();
  }

  cargarFecha(){
    this.yearRange = "1994" + ":" +(new Date().getFullYear().toString());    
  }

  devolverCampos(){
    let valores:string[]=[];
    let fchIni:string=this.validarFechaInical();
    let fchFin:string=this.validarFechaFinal();
    (fchIni != "") ? valores.push(fchIni):"";
    (fchFin != "") ? valores.push(fchFin):"";
    this.ngFechaInicial=null;
    this.ngFechaFinal=null;
    this.valoresFiltroDescarga.emit(valores);
  }

  cargarMesesEsp(){
    this.es = {
      firstDayOfWeek: 1,
      dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
      dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
      dayNamesMin: [ "D","L","M","X","J","V","S" ],
      monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
      monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
      today: 'Hoy',
      clear: 'Borrar'
    }
  }

  fechaMaxMin(){
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let nextMonth = (month === 11) ? 0 : month ;
    let nextYear = (nextMonth === 0) ? year: year;
    let prevMonth = (month === 0) ? 11 : month ;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    this.maxDate = new Date();
    this.maxDate.setMonth(nextMonth);
    this.maxDate.setFullYear(nextYear);
    this.minDate = new Date();
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);    
  }

  validarDatosFechaFinal(){
    this.ngFechaFinal = null;
    this.ngFechaInicial = (this.ngFechaInicial !==undefined && this.ngFechaInicial !== null) ? this.ngFechaInicial : new Date();
    let today = this.ngFechaInicial;
    let day = today.getDate();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    this.minDate.setDate(day);
    this.minDate.setMonth(prevMonth);
    this.minDate.setFullYear(prevYear);
  }

  validarFechaFinal(){
    if(this.ngFechaFinal !==undefined && this.ngFechaFinal !== null){
      let diaFchFin = (this.ngFechaFinal.getDate() < 10 )?"0"+this.ngFechaFinal.getDate():this.ngFechaFinal.getDate().toString();
      let mesFchFin = ((this.ngFechaFinal.getMonth()+1) < 10)?"0"+(this.ngFechaFinal.getMonth()+1):(this.ngFechaFinal.getMonth()+1).toString();
      let anioFchFin = this.ngFechaFinal.getFullYear().toString();
      return (anioFchFin+mesFchFin+diaFchFin);
    }else{
      return "";
    }
  }

  validarFechaInical(){
    if(this.ngFechaInicial !==undefined && this.ngFechaInicial !== null){
      let diaFchIni = (this.ngFechaInicial.getDate() < 10 )?"0"+this.ngFechaInicial.getDate():this.ngFechaInicial.getDate().toString();
      let mesFchIni = ((this.ngFechaInicial.getMonth()+1) < 10)?"0"+(this.ngFechaInicial.getMonth()+1):(this.ngFechaInicial.getMonth()+1).toString();
      let anioFchIni = this.ngFechaInicial.getFullYear().toString();
      return (anioFchIni+mesFchIni+diaFchIni);
    }else{
      return "";
    }
  }
}
