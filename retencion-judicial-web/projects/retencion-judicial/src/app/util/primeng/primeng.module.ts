import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { KeyFilterModule } from 'primeng/keyfilter';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    DropdownModule,
    FormsModule,
    KeyFilterModule,
    PaginatorModule,
    TableModule,
    ToastModule,
    DynamicDialogModule,
    DialogModule,
    CalendarModule,
    RadioButtonModule
  ],
  exports: [
    BrowserAnimationsModule,
    DropdownModule,
    FormsModule,
    KeyFilterModule,
    PaginatorModule,
    TableModule,
    ToastModule,
    DynamicDialogModule,
    DialogModule,
    CalendarModule,
    RadioButtonModule
  ]
})
export class PrimengModule { }
