import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatoJuzgadoComponent } from './dato-juzgado.component';

describe('DatoJuzgadoComponent', () => {
  let component: DatoJuzgadoComponent;
  let fixture: ComponentFixture<DatoJuzgadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatoJuzgadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatoJuzgadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
