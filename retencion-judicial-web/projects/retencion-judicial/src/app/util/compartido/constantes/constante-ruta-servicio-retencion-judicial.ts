import { environment } from 'projects/retencion-judicial/src/environments/environment';

//Serivicios
export const LISTAR_RETENCINES = 'listar-retenciones';
export const DESCARGAR_RETENCIONES_JUDICIALES = 'descargar-retencines-judiciales';
export const LISTAR_TIPO_MONEDA = 'listar-tipo-moneda';
export const LISTAR_MODALIDA_PENSION = 'listar-modalidad-pension';
export const LISTAR_ENTIDAD_ASEGURADORA = 'listar-entidad-aseguradora';

export const LISTAR_TIPO_IDENTIFICACION = 'listar-tipo-identificacion';
export const LISTAR_TIPO_SEXO = 'listar-sexo';
export const ACTUALIZAR_RETENCION_JUDICIAL = 'actualizar-retencion';
export const DEJAR_SIN_EFECTO = 'dejar-sin-efecto';
export const LISTAR_TIPO_PROCESO = 'listar-tipo-proceso';
export const LISTAR_BENEFICIARIO = 'listar-beneficiario';
export const LISTAR_PENSIONISTA = 'listar-pensionista';
export const LISTAR_AFILIADO = 'listar-afiliado';
export const LISTAR_DEPARTAMENTO = 'listar-depatamento';
export const LISTAR_CIUDAD_PROVINCIA = 'listar-ciudad-provincia';
export const LISTAR_DISTRITO = 'listar-distrito';
export const LISTAR_AGENCIAS = 'listar-agencias';
export const LISTA_INDICADOR_TIPO_PROCESO = 'listar-indicador-tipo-proceso';
export const LISTA_TIPO_AFECTACION = 'listar-tipo-afectacion';
export const NUEVA_RETENCION = 'nueva-retencion';
export const VALIDAR_PENSIONISTA = 'validar-pensionista';
export const LISTA_FORMA_PAGO = 'listar-forma-pago';
export const LISTA_ENCARGAR_RETENCION = 'listar-encargado-retencion';
export const LISTA_MONTO_FECHA = 'listar-monto-fecha';
export const LISTA_EXISTE_LIMITE = 'listar-existe-limite';
export const LISTA_MONTO_PORCENTAJE = 'listar-monto-porcentaje';
export const LISTA_ENVIAR_JUZGADO = 'lista-enviar-juzgado';

export const LISTAR_ENTIDADES_FINANCIERAS_DEPOSITO = 'listar-bancos-deposito';
export const LISTAR_ENTIDADES_FINANCIERAS_VENTANILLA = 'listar-bancos-ventanilla';
export const LISTAR_ENTIDADES_FINANCIERAS_CHEQUE_CONSIGNACION_JUDICIAL = 'listar-bancos-cheque-judicial';

//URL
export const URL_LISTAR_RETENCIONES = environment.url_servicio_retencion_judicial;
export const URL_DATOS_GENERALES = environment.url_servicio_dato_general;

//Access-Control-Allow-Origin
export const ACCCES_CONTROL_ALLOW_ORIGIN = environment.control_allow_origin;

//Otros
export const TITULO_DE_CONFIRMACION_GENERICO = environment.titulo_confirmacion_generico;
export const MENSAJE_DE_CONFIRMACION_GENERICO = environment.mensaje_confirmacion_generico;
export const TITULO_DE_ERROR_GENERICO = environment.titulo_error_generico;
export const MENSAJE_DE_ERROR_GENERICO = environment.mensaje_error_generico;
export const TIPO_DE_PROCESO = environment.tipo_proceso;
export const BANCO_SCOTIABANK = environment.banco_scotiabank;
export const BANCO_BCP = environment.banco_bcp;
export const BANCO_INTERBANK = environment.banco_interbank;
export const BANCO_CONTINENTAL = environment.banco_continental;
export const BANCO_DE_LA_NACION = environment.banco_de_la_nacion;
export const AGENTE_AFP_INTEGRA = environment.afp_integra;
export const AGENCIA_PRINCIPAL = environment.afp_integra_agencia_princial;
