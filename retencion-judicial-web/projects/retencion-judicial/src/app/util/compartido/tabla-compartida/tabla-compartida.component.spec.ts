import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaCompartidaComponent } from './tabla-compartida.component';

describe('TablaCompartidaComponent', () => {
  let component: TablaCompartidaComponent;
  let fixture: ComponentFixture<TablaCompartidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaCompartidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaCompartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
