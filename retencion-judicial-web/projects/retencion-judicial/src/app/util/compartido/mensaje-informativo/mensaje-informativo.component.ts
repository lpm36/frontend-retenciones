import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mensaje-informativo',
  templateUrl: './mensaje-informativo.component.html',
  styleUrls: ['./mensaje-informativo.component.css']
})
export class MensajeInformativoComponent implements OnInit {

  @Input() mensajeError: string;
  @Input() mensajeInformativo: string;

  constructor() { }

  ngOnInit() {
  }

}
