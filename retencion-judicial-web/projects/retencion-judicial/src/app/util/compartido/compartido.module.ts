import { NgModule } from '@angular/core';
import { TablaCompartidaComponent } from './tabla-compartida/tabla-compartida.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { PrimengModule } from '../primeng/primeng.module';
import { DatoJuzgadoComponent } from './dato-juzgado/dato-juzgado.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MensajeInformativoComponent } from './mensaje-informativo/mensaje-informativo.component';


@NgModule({
  declarations: [
    TablaCompartidaComponent,
    PageNotFoundComponentComponent, DatoJuzgadoComponent, MensajeInformativoComponent
  ],
  exports: [
    TablaCompartidaComponent,
    PageNotFoundComponentComponent,
    MensajeInformativoComponent
  ],
  imports: [
    MaterialModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CompartidoModule { }
