export class RetencionDistrito {
  codDepartamento: string;
  codCiudad: string;
  codDistrito: string;
  nombre: string;
  codPostal: string;
  estado: string;
}
