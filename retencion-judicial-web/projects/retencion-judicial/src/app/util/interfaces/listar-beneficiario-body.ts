export class ListarBeneficiarioBody {
  benePriNombre: string;
  beneSegNombre: string;
  beneApePaterno: string;
  beneApeMaterno: string;
  beneNumDoc: string;
  benPagina: number;
  beneCuspp: string;
  beneCantPorPagina: number;
  beneTotal: boolean;
}
