export class RetencionAfiliado {
  cuspp: string;
  numDocumento: string;
  estadoAfiliado: string;
  primerApe: string;
  segundoApe: string;
  primerNom: string;
  segundoNom: string;
  fechaNaci: string;
  tipoAfiliado: string;
  sexo: string;
  afilBene: string;
  cusppLargo: string;
  apellidoPaterLar: string;
  apellidoMaterLar: string;
  primerNombLar: string;
  segundoNombLar: string;
  tercerApeLar: string;
  tercerNombLar: string;
}
