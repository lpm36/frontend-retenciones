export interface MedioPagoModificar {
  indicadorLimUsar: string;
  monedaRetencion: string;
  monedaPado: string;
  topeLimite: string;
  topeMontoPorsentaje: string;
  porcentajeDescuento: number;
  montoDescuento: number;
  montoTope: number;
  banco: string;
  nroCuentaDesposi: string;
  formaPagoRetencion: string;
  ciaSeguroCargoReten: string;
  cartaRespuEnvJuzga: string;
  encargadoRetencion: string;
}
