export interface RetencionJudicialLista {

  Pro:String;
  Afectado:String;
  Secuencia:number;
  Cuspp:String;
  DNI:String;
  Nombre_Demandado:String;
  Cuspp2:String;
  Nombre_alimentista:String;
  FechaR:String;
  Estado:String;
}
