export class retencionPensionistaAfp {
  cuspp: string;
  tipoBeneficiario: string;
  descTipo: string;
  numBeneficiario: string;
  secuPensionado: string;
  estadoPensionado: string;
  tipoSolicitud: string;
  subestPension: string;
  modalidadPension: string;
  retencionPago: string;
  montoAprobadoPension: number;
  montoActualPension: number;
}
