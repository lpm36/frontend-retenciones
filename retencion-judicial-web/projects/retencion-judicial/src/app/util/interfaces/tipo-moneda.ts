export interface TipoMoneda {

  columValor: string;
  valor: string;
  descripcion: string;

}
