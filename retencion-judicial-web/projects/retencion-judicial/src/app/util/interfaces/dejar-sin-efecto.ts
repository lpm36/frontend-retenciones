export class DejarSinEfecto {
  numOficio: string;
  numExpediente: string;
  numSac: string;
  nombreJuzgado: string;
  observacion: string;
  usuario: string;
  tipoProceso: string;
  numIdent: string;
  secuencia: number;
}
