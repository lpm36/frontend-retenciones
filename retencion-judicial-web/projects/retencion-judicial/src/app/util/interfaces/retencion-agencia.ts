export class RetencionAgencia {
  codigoAgencia: string;
  nombreAgencia: string;
  direccionAgencia: string;
  nombreAgente: string;
  telefono: string;
  fax: string;
  estado: string;
  periodoPres: string;
  codigoSucursal: string;
  codigoDepto: string;
  codigoCiudad: string;
  tipoComision: string;
}
