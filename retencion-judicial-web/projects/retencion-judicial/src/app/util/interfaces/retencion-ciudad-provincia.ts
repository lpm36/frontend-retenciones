export class RetencionCiudadProvincia {
  codDepartemento: string;
  codCiudad: string;
  codRegion: string;
  nombreCiudad: string;
  codArea: string;
  estadoRegistro: string;
}

