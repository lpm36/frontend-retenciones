export class ListarAfiliadoBody {
  priNombre: string;
  segNombre: string;
  apePaterno: string;
  apeMaterno: string;
  numDoc: string;
  pagina: number;
  cuspp: string;
  cantPorPagina: number;
  total: boolean;
}
