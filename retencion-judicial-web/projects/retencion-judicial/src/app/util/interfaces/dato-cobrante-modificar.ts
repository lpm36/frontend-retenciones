export interface DatoCobranteModificar {
  apePaterno: string;
  apeMaterno: string;
  primerNombreCobrante: string;
  segundNombreCobrante: string;
  sexoCobrante: string;
  tipoDocCobrante: string;
  nroDocCobrante: string;
  cuspp: string;
  sac: string;
}
