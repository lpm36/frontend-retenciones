/**
 * DEMANDADO -> ALIFLIADO O BENEFICIARIO - DATOS PARA GENERAR OTROS PROCESOS EN AS400
 * DEMANDANTE -> LA PERSONA QUE DEMANDA - DATOS REFERENCIALES NO IMPORTANTE
 * COBRANTE -> LA PERSONA QUIEN SE SUPONE QUE COBRARA EL DINERO  - DATOS REFERENCIALES
 * ALIMENTISTA -> PARA EL AS400 ES EL BENEFICIAIRO - DATOS PARA GENERAR OTROS PROCESOS EN AS400
 * LA CLASE "RetigistroRetencionJudicial" ES DIFERENCTE A LA "RetencionJudicial"
 * NO SON IGUALES, TENER EN CUENTA QUE "RetigistroRetencionJudicial" SOLO SE UTILIZA PARA GENERAR EL BODY QUE SE ENVIARA PARA REGISTRAR
 * PARA EL DETALLE :
 * DATOS DEL ALIMENTISTA -> DEMANDANTE
 * DATOS DEL DEMANDANTE -> QUIEN_DEMANDA
 * ----------------------------------------------------------
 * Se tienen cuatro campos en AS400:
 * Afiliado(que ahi va los datos del Beneficiario, si es que llega ser beneficiario)
 * Demandante(Que para nostros es el Alimentista y que son necesarios para los demas proceso de AS400)
 * QuienDemanda(Que para nosotros son los datos del Demandante)
 * Cobrante(Que son los datos del Cobrante)
 */
export class RetencionJudicial {
  tipoProceso: string;
  numIdentificaion: string;
  secuencia: number;
  afecta: string;
  apellidoPaternoAfiliado: string;
  apellidoMaternoAfiliado: string;
  primerNombreAfiliado: string;
  segundoNombreAfiliado: string;
  tipoDocAfiliado: string;
  numDocAfiliado: string;
  sexoAfiliado: string;
  tipoBeneficiario: string;
  numBeneficiario: number;
  secBeneficiario: number;
  ingGeneraReintegro: string;
  apePaternoDemantante: string;
  apeMaternoDemandante: string;
  primerNombreDemandante: string;
  encardoRetencion: string;
  segundoNombreDemandante: string;
  tipoDocDemandante: string;
  numDocDemandante: string;
  sexoDemandante: string;
  numOficio: string;
  juzgado: string;
  indicadorHastaMontoFecha: string;
  montoTope: number;
  porcentajeDescuento: number;
  fchInicioDescuento: string;
  fchFinDescuento: string;
  montoDescuento: number;
  cuotasArmadas: number;
  formaPago: string;
  banco: string;
  numCheque: string;
  numCuentaDeposito: string;
  fchRecepcionOficio: string;
  agenciaRecibeAtencion: string;
  fchEmisionCartaRespuesta: string;
  observacion1: string;
  observacion2: string;
  observacion3: string;
  estadoRetencion: string;
  dirigidoA: string;
  nombreJuez: string;
  lugar: string;
  corte: string;
  tipoCargo: string;
  numCargo: number;
  estadoCargo: string;
  fchRecepcionCargo: string;
  fchCancelacion: string;
  horaCancelacion: string;
  usuarioCancelacion: string;
  indicacor1: string;
  indicacor2: string;
  indicacor3: string;
  fchCreacion: string;
  horaCreacion: string;
  usuarioCreacion: string;
  fchModificacion: string;
  horaModificacion: string;
  usuarioModificacion: string;
  solicitudBeneficioCuspp: string;
  solicitudBeneficioAutogenerado: number;
  fchCreacionSolicitudBeneficiario: string;
  horaCreacionSolicitudBeneficiario: string;
  usuarioCreacionSolicitudBeneficiario: string;
  fchApr: string;
  horaApr: string;
  usuarioApr: string;
  fchCna: string;
  horaCna: string;
  usuarioCna: string;
  cusppBeneficiario: string;
  responsablePago: string;
  quienResponsablePago: string;
  comentario: string;
  monedaRetencion: string;
  monedaPago: string;
  tipoTope: string;
  existeTopeLimite: string;
  cartaRespuestaEnvJuz: string;
  numeroSacReclamo: string;

  fchModificacionWeb: string;
  horaModificacionWeb: string;
  usuarioModificacionWeb: string;
  apePatQuienDemandante: string;
  apeMatQuienDemandante: string;
  priNomQuienDenmandate: string;
  segNomQuienDemandante: string;
  tipDocQuienDemandante: string;
  numDocQuienDemandante: string;
  sexoQuienDemandante: string;
  cusppQuienDemandante: string;
  sacQuienDemandante: string;
  apePatCobrante: string;
  apeMatCobrante: string;
  priNomCobrante: string;
  segNomCobrante: string;
  tipDocCobrante: string;
  numDocCobrante: string;
  sexoCobrante: string;
  cusppCobrante: string;
  sacCobrante: string;
  paisJuzgado: string;
  departamentoJuzgado: string;
  provinciaKJuzgado: string;
  distritoJuzgado: string;
  numExpedite: string;
  fchOficio: string;
  envConsultaJuzgado: string;
  tipoProcesoRete: string;
  tipoAfeactacion: string;
  ciaSeguros: string;
}
