export class RetencionDepartamento {
  codigoDept: string;
  nombreDept: string;
  agencia: string;
  registro: string;
}
