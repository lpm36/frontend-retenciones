export interface EntidadFinanciera {
  codEntRec: string;
  nombreEntRec: string;
  direccion: string;
  responsable: string;
  telefono: string;
  numOficina: string;
}
