export interface EntidadAseguradora {
  codEntiAsegu: string;
  nombEmtiAsegu: String;
  responsable: string;
  direccion: string;
  telefono: string;
  fax: string;
  tipo: string;
  cargoResponsab: string;
  numIdentifcacion: string
  abreviatura: string;
  urbanizacion: string;
  codTercero: string;
  email: string;
  codDepto: string;
  codCiudad: string;
  codDistrito: string;
  tipoIdentificacion: string;
}
