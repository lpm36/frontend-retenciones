export class RetencionBeneficiario {
  cuspp: string;
  tipo: string;
  descTipo: string;
  numBeneficiario: string;
  primerApe: string;
  segundoApe: string;
  primerNom: string;
  segundoNom: string;
  sexo: string;
  numDocumento: string;
  rolSolicitud: string;
  estadoSolicitud: string;
  ciaSeguros: string;
}
