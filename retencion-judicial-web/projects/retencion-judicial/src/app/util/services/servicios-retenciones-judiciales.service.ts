import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { URL_LISTAR_RETENCIONES, LISTAR_RETENCINES, DESCARGAR_RETENCIONES_JUDICIALES, URL_DATOS_GENERALES, LISTAR_TIPO_MONEDA, LISTAR_ENTIDAD_ASEGURADORA, LISTAR_TIPO_IDENTIFICACION, LISTAR_TIPO_SEXO, ACTUALIZAR_RETENCION_JUDICIAL, DEJAR_SIN_EFECTO, LISTAR_TIPO_PROCESO, LISTAR_BENEFICIARIO, LISTAR_PENSIONISTA, LISTAR_AFILIADO, LISTAR_DEPARTAMENTO, LISTAR_CIUDAD_PROVINCIA, LISTAR_DISTRITO, LISTAR_AGENCIAS, LISTA_INDICADOR_TIPO_PROCESO, LISTA_TIPO_AFECTACION, NUEVA_RETENCION, VALIDAR_PENSIONISTA, LISTAR_ENTIDADES_FINANCIERAS_DEPOSITO, LISTAR_ENTIDADES_FINANCIERAS_CHEQUE_CONSIGNACION_JUDICIAL, LISTAR_ENTIDADES_FINANCIERAS_VENTANILLA, LISTA_FORMA_PAGO, LISTA_ENCARGAR_RETENCION, LISTA_MONTO_FECHA, LISTA_EXISTE_LIMITE, LISTA_MONTO_PORCENTAJE, LISTA_ENVIAR_JUZGADO } from '../compartido/constantes/constante-ruta-servicio-retencion-judicial';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TipoMoneda } from '../interfaces/tipo-moneda';
import { EntidadAseguradora } from '../interfaces/entidad-aseguradora';
import { EntidadFinanciera } from '../interfaces/entidad-financiera';
import { TipoIdentificacion } from '../interfaces/tipo-identificacion';
import { ListaValor } from '../interfaces/lista-valor';
import { ActualizarDatoRetencionJudicial } from '../interfaces/actualizar-datos-retencion';
import { DejarSinEfecto } from '../interfaces/dejar-sin-efecto';
import { RetencionBeneficiario } from '../interfaces/retencion-beneficiario';
import { retencionPensionistaAfp } from '../interfaces/retencion-pensionista-afp';
import { RetencionAfiliado } from '../interfaces/retencion-afiliado';
import { RetencionDepartamento } from '../interfaces/retencion-departamento';
import { RetencionCiudadProvincia } from '../interfaces/retencion-ciudad-provincia';
import { RetencionDistrito } from '../interfaces/retencion-distrito';
import { RetencionAgencia } from '../interfaces/retencion-agencia';
import { RetigistroRetencionJudicial } from '../interfaces/retigistro-retencion-judicial';
import { ListarAfiliadoBody } from '../interfaces/listar-afiliado-body';
import { ListarBeneficiarioBody } from '../interfaces/listar-beneficiario-body';

@Injectable({
  providedIn: 'root'
})
export class ServiciosRetencionesJudicialesService {

  constructor(private http: HttpClient) { }

  getListarRetenciones(prNom: string = '', sgNom: string = '', apPat: string = '', apMat: string = '', numDoc: string = '', pagina: number = 0, total: boolean = true): Observable<any> {
    const url = URL_LISTAR_RETENCIONES.concat(LISTAR_RETENCINES);
    const params = new HttpParams()
      .set('prNom', prNom.trim().toLocaleUpperCase())
      .set('sgNom', sgNom.trim().toLocaleUpperCase())
      .set('apPat', apPat.trim().toLocaleUpperCase())
      .set('apMat', apMat.trim().toLocaleUpperCase())
      .set('numDoc', numDoc.trim())
      .set('pagina', pagina.toString())
      .set('total', total.toString());
    return this.http.get(url, { params });
  }
  /*
    getListarRetenciones(pagina: number = 0, nombre: string = '', dni: string = '', total: boolean = true): Observable<any> {
      const url = URL_LISTAR_RETENCIONES.concat(LISTAR_RETENCINES);
      const params = new HttpParams()
        .set('pagina', pagina.toString())
        .set('nombre', nombre.trim())
        .set('dni', dni.trim())
        .set('total', total.toString());
      return this.http.get(url, { params });
    }*/

  getDescargarRetencionesJudiciales(fchInicio: number, fchFin: number, estado: string): Observable<any> {
    const url = URL_LISTAR_RETENCIONES.concat(DESCARGAR_RETENCIONES_JUDICIALES);
    const params = new HttpParams()
      .set('fchInicial', fchInicio.toString())
      .set('fchFinal', fchFin.toString())
      .set('estado', estado);
    return this.http.get(url, { params });
  }

  getDescargarRetJud(fchInicio: number, fchFin: number, estado: string): Observable<any> {
    const url = URL_LISTAR_RETENCIONES.concat(DESCARGAR_RETENCIONES_JUDICIALES);
    const params = new HttpParams()
      .set('fchInicial', fchInicio.toString())
      .set('fchFinal', fchFin.toString())
      .set('estado', estado);
    return this.http.get(url, { params, responseType: 'arraybuffer' });
  }

  getTipoMonde(): Observable<TipoMoneda[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_TIPO_MONEDA);
    return this.http.get<TipoMoneda[]>(url);
  }

  getEntidadAseguradoras(pagina: number = 0, codAseguradora: string = '', codDepartamento: string = '',
    codCiudad: string = '', codDistrito: string = ''): Observable<EntidadAseguradora[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_ENTIDAD_ASEGURADORA);
    const params = new HttpParams()
      .set('pagina', pagina.toString())
      .set('cod_aseguradora', codAseguradora.trim())
      .set('cod_departamento', codDepartamento.trim())
      .set('cod_ciudad', codCiudad.trim())
      .set('cod_distrito', codDistrito.trim());

    return this.http.get<EntidadAseguradora[]>(url, { params });
  }

  getEntidadFinanciearDeposito(): Observable<EntidadFinanciera[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_ENTIDADES_FINANCIERAS_DEPOSITO);
    return this.http.get<EntidadFinanciera[]>(url);
  }

  getEntidadFinanciearChequeConsignacionJudicial(): Observable<EntidadFinanciera[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_ENTIDADES_FINANCIERAS_CHEQUE_CONSIGNACION_JUDICIAL);
    return this.http.get<EntidadFinanciera[]>(url);
  }

  getEntidadFinanciearVentanilla(): Observable<EntidadFinanciera[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_ENTIDADES_FINANCIERAS_VENTANILLA);
    return this.http.get<EntidadFinanciera[]>(url);
  }

  getTipoIdentificacion(pagina: number = 0): Observable<TipoIdentificacion[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_TIPO_IDENTIFICACION);
    const params = new HttpParams().set('pagina', pagina.toString());
    return this.http.get<TipoIdentificacion[]>(url, { params });
  }

  getTipoSexo(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_TIPO_SEXO);
    return this.http.get<ListaValor[]>(url);
  }

  postActualizarRetencionJudicial(actualizarDatosRetencion: ActualizarDatoRetencionJudicial): Observable<number> {
    const url = URL_LISTAR_RETENCIONES.concat(ACTUALIZAR_RETENCION_JUDICIAL);
    return this.http.post<number>(url, actualizarDatosRetencion);
  }

  postDejarSinEfecto(dejarSinEfecto: DejarSinEfecto): Observable<number> {
    const url = URL_LISTAR_RETENCIONES.concat(DEJAR_SIN_EFECTO);
    return this.http.post<number>(url, dejarSinEfecto);
  }

  getTipoProceso(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_TIPO_PROCESO);
    return this.http.get<ListaValor[]>(url);
  }

  postListarBeneficiario(listarAfiliadoBody: ListarBeneficiarioBody): Observable<RetencionBeneficiario[]> {
    const url = URL_LISTAR_RETENCIONES.concat(LISTAR_BENEFICIARIO);
    return this.http.post<RetencionBeneficiario[]>(url, listarAfiliadoBody);
  }

  getListarPensinista(cuspp: string = '', tipo: string = '9', numBen: number = -1, pagina: number = 0, cantPorPag: number = 10, total: boolean = true): Observable<retencionPensionistaAfp[]> {
    const url = URL_LISTAR_RETENCIONES.concat(LISTAR_PENSIONISTA);
    const params = new HttpParams()
      .set('cuspp', cuspp.trim())
      .set('tipo', tipo.trim())
      .set('numBen', numBen.toString())
      .set('pagina', pagina.toString())
      .set('cant_por_pagina', cantPorPag.toString()).set('total', total.toString());
    return this.http.get<retencionPensionistaAfp[]>(url, { params });
  }

  postListarAfiliado(listarAfiliadoBody: ListarAfiliadoBody): Observable<RetencionAfiliado[]> {
    const url = URL_LISTAR_RETENCIONES.concat(LISTAR_AFILIADO);
    return this.http.post<RetencionAfiliado[]>(url, listarAfiliadoBody);
  }

  getListarDepartamento(cod_departamento: string = '', pagina: number = 0): Observable<RetencionDepartamento[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_DEPARTAMENTO);
    const params = new HttpParams().set('cod_departamento', cod_departamento.trim()).set('pagina', pagina.toString());
    return this.http.get<RetencionDepartamento[]>(url, { params });
  }

  getCiudadProvincia(cod_departamento: string = '', cod_ciudad: string = '', cod_region: string = '', pagina: number = 0): Observable<RetencionCiudadProvincia[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_CIUDAD_PROVINCIA);
    const params = new HttpParams().set('cod_departamento', cod_departamento.trim())
      .set('cod_ciudad', cod_ciudad.trim())
      .set('cod_region', cod_region.trim())
      .set('pagina', pagina.toString());
    return this.http.get<RetencionCiudadProvincia[]>(url, { params });
  }

  getDistrito(cod_distrito: string = '', cod_ciudad: string = '', cod_departamento: string = '', cod_postal: string = '', pagina: number = 0): Observable<RetencionDistrito[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_DISTRITO);
    const params = new HttpParams()
      .set('cod_distrito', cod_distrito.trim())
      .set('cod_ciudad', cod_ciudad.trim())
      .set('cod_departamento', cod_departamento.trim())
      .set('cod_postal', cod_postal.trim())
      .set('pagina', pagina.toString());
    return this.http.get<RetencionDistrito[]>(url, { params });
  }

  getListarAgencias(pagina: number = 0, cod_agencia: string = '', cod_sucursal: string = '', cod_departamento: string = '', cod_ciudad: string = ''): Observable<RetencionAgencia[]> {
    const url = URL_DATOS_GENERALES.concat(LISTAR_AGENCIAS);
    const params = new HttpParams()
      .set('pagina', pagina.toString())
      .set('cod_agencia', cod_agencia)
      .set('cod_sucursal', cod_sucursal)
      .set('cod_departamento', cod_departamento)
      .set('cod_ciudad', cod_ciudad);
    return this.http.get<RetencionAgencia[]>(url, { params });
  }

  getListaIndicadorTipoProceso(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_INDICADOR_TIPO_PROCESO);
    return this.http.get<ListaValor[]>(url);
  }

  getListaTipoAfectacion(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_TIPO_AFECTACION);
    return this.http.get<ListaValor[]>(url);
  }

  getListaFormaPago(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_FORMA_PAGO);
    return this.http.get<ListaValor[]>(url);
  }

  getListaEncargadoRetencion(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_ENCARGAR_RETENCION);
    return this.http.get<ListaValor[]>(url);
  }

  getListaMontoFecha(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_MONTO_FECHA);
    return this.http.get<ListaValor[]>(url);
  }

  getListaExisteLimite(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_EXISTE_LIMITE);
    return this.http.get<ListaValor[]>(url);
  }

  getListaMontoPorcentaje(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_MONTO_PORCENTAJE);
    return this.http.get<ListaValor[]>(url);
  }

  getListaEnviarJuzgado(): Observable<ListaValor[]> {
    const url = URL_DATOS_GENERALES.concat(LISTA_ENVIAR_JUZGADO);
    return this.http.get<ListaValor[]>(url);
  }

  postNuevaRetencion(retigistroRetencionJudicial: RetigistroRetencionJudicial): Observable<any> {
    const url = URL_LISTAR_RETENCIONES.concat(NUEVA_RETENCION);
    return this.http.post<any>(url, retigistroRetencionJudicial);
  }

  getValidarPensionista(cuspp: string = '', numDoc: string = ''): Observable<number> {
    const url = URL_LISTAR_RETENCIONES.concat(VALIDAR_PENSIONISTA);
    const params = new HttpParams()
      .set('numDoc', numDoc)
      .set('cuspp', cuspp);
    return this.http.get<number>(url, { params });
  }
}
