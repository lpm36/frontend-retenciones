import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ACCCES_CONTROL_ALLOW_ORIGIN } from '../compartido/constantes/constante-ruta-servicio-retencion-judicial';

@Injectable()
export class headerInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler) {

    const token: string = localStorage.getItem('token');

    if (token) {
      request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    request = request.clone({ headers: request.headers.set('Access-Control-Allow-Origin', ACCCES_CONTROL_ALLOW_ORIGIN) });
    request = request.clone({ headers: request.headers.set('Access-Control-Allow-Credentials', 'true') });
    request = request.clone({ headers: request.headers.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE') });
    request = request.clone({ headers: request.headers.set('Access-Control-Max-Age', '3600') });
    request = request.clone({ headers: request.headers.set('Access-Control-Allow-Headers', 'Content-Type, Accept, X-Requested-With, remember-me') });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        /*if (event instanceof HttpResponse) {

        }*/
        return event;
      }));
  }
}
