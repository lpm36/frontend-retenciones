import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarRetencionesComponent } from './inicio/listar-retenciones/listar-retenciones.component';
import { PageNotFoundComponentComponent } from './util/compartido/page-not-found-component/page-not-found-component.component';
import { NuevaRetencionComponent } from './inicio/listar-retenciones/nueva-retencion/nueva-retencion.component';
import { ExportarRetencionComponent } from './inicio/exportar-retencion/exportar-retencion.component';
import { DetalleRetencionComponent } from './inicio/detalle-retencion/detalle-retencion.component';
import { VistaRetencionesComponent } from './inicio/vista-retenciones/vista-retenciones.component';

const routes: Routes = [
  { path: 'listar-retenciones', component: ListarRetencionesComponent },
  { path: 'nueva-retencion', component: NuevaRetencionComponent },
  { path: 'exportar-retencion', component: ExportarRetencionComponent },
  { path: 'detalle-retencion', component: DetalleRetencionComponent },
  { path: 'vista-retenciones', component: VistaRetencionesComponent },
  { path: '', redirectTo: 'detalle-retencion', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
