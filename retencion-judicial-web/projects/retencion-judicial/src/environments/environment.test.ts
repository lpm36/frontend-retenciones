export const environment = {
  production: true,
  url_servicio_retencion_judicial: 'http://internal-lbt-app-beneficio-576577970.us-west-2.elb.amazonaws.com/beneficio/retencion/judicial/',
  url_servicio_persona: '',
  url_servicio_dato_general: 'http://internal-lbt-integra-api-core-1521719273.us-west-2.elb.amazonaws.com/soporte/dato-general/',
  control_allow_origin: '*',
  titulo_confirmacion_generico: 'Confirmación',
  mensaje_confirmacion_generico: 'El proceso fue realizado exitosamente',
  titulo_error_generico: 'Error',
  mensaje_error_generico: 'Ocurrio un error, por favor verifique los datos y vuelva intentar',
  tipo_proceso: 'PRJ',
  banco_scotiabank: '10009',
  banco_bcp: '10002',
  banco_interbank: '10003',
  banco_continental: '10011',
  banco_de_la_nacion: '10080',
  afp_integra: '99999',
  afp_integra_agencia_princial: '999'
};
